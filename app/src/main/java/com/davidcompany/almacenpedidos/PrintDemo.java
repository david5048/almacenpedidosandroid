package com.davidcompany.almacenpedidos;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.zj.btsdk.BluetoothService;
import com.zj.btsdk.PrintPic;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import static com.davidcompany.almacenpedidos.PrinterCommands.CHAR_SET;


public class PrintDemo extends Activity {
	Button btnSearch;
	Button btnSendDraw;
	Button btnSend;
	Button btnClose;
	Button btnIrMenu;
	EditText edtContext;
	EditText edtPrint;
	private static final int REQUEST_ENABLE_BT = 2;
	BluetoothService mService = null;
	BluetoothDevice con_dev = null;
	private static final int REQUEST_CONNECT_DEVICE = 1;  //Recibe noticias sobre dispositivos
	private int conn_flag = 0;
	private ConnectPaireDev mConnPaireDev = null;
	Activity activity;
	
	
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		mService = new BluetoothService(this, mHandler);
		activity = this;
		//Bluetooth no esta disponible, salga del programa
		if( mService.isAvailable() == false ){
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
		}		
	}

    @Override
    public void onStart() {
    	super.onStart();
    	//Bluetooth no esta encendido, encienda Bluetooth
		if( mService.isBTopen() == false)
		{
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		}
		try {
			btnSendDraw = (Button) this.findViewById(R.id.btn_test);
			btnSendDraw.setOnClickListener(new ClickEvent());
			btnSearch = (Button) this.findViewById(R.id.btnSearch);
			btnSearch.setOnClickListener(new ClickEvent());
			btnSend = (Button) this.findViewById(R.id.btnSend);
			btnSend.setOnClickListener(new ClickEvent());
			btnClose = (Button) this.findViewById(R.id.btnClose);
			btnClose.setOnClickListener(new ClickEvent());
			btnIrMenu = (Button) this.findViewById(R.id.btnMenuPrincipal);
			btnIrMenu.setOnClickListener(new ClickEvent());
			edtContext = (EditText) findViewById(R.id.txt_content);
			btnClose.setEnabled(false);
			btnSend.setEnabled(false);
			btnSendDraw.setEnabled(false);
		} catch (Exception ex) {
            Log.e("Mensaje de error",ex.getMessage());
		}

		mConnPaireDev = new  ConnectPaireDev();
		mConnPaireDev.start();
    }
    
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mService != null)
			mService.stop();
		mService = null;
	}

	public byte[] printPhoto() {
		try {
			Bitmap bmp = BitmapFactory.decodeResource(getResources(),R.drawable.logokaliopeticketjustificacionizq);

			if(bmp!=null){
				byte[] command = Utils.decodeBitmap(bmp);
				Log.d("Print Photo", "size " + command.length);
				return command;
			}else{
				Log.e("Print Photo error", "the file isn't exists");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("PrintTools", "the file isn't exists");
		}
		return null;
	}
	
	class ClickEvent implements View.OnClickListener {
		public void onClick(View v) {
			if (v == btnSearch) {			
				Intent serverIntent = new Intent(PrintDemo.this,DeviceListActivity.class);      //Ejecuta otro tipo de actividad
				startActivityForResult(serverIntent,REQUEST_CONNECT_DEVICE);

			} else if (v == btnSend) {

				imprimirEtiqueta();




			} else if (v == btnClose) {
				mService.stop();
			} else if (v == btnSendDraw) {

				mService.sendMessage(PrinterCommands.home(),CHAR_SET);
            	}

			else if (v == btnIrMenu) {
				new AlertDialog.Builder(activity)
						.setTitle("�Quieres ir a menu?")
						.setMessage("Se te enviara al menu principal\n\n�Ya imprimiste las etiquetas necesarias?")
						.setPositiveButton("Si ya termine", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								startActivity(new Intent(activity, MainActivity.class));
							}
						})
						.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {

							}
						}).create().show();

			}



			}

	}

	private void imprimirEtiqueta() {
		String empresa = "Kaliope Mexico S.A.C.V";
		String cuenta = "";
		String nombreCliente = "";
		String limiteCredito = "";
		String grado = "";
		String diasCredito = "";
		String ruta = "";
		String sucursal = "";
		String asignado = "";
		String fechaEntrega = "";
		String piezasTotales = "";
		String piezasCredito = "";
		String importeCredito = "";
		String piezasInversion = "";
		String importeInversion = "";
		String gananciaTotal = "";

		String titulo = "";
		String difCredito = "";
		String difCreditoMessage = "";
		String pedidoPagoCredito = "";
		String pagoCreditoMessage = "";
		String pagoInversionMessage = "";
		String pagoTotal = "";
		String pagoTotalMessage = "";
		String cantidadFinanciada = "";
		String cantidadFinanciadaMessage = "";






		try {
			cuenta = DetallesActivity.totales.getString("cuenta");
			nombreCliente = DetallesActivity.totales.getString("nombre");
			limiteCredito = DetallesActivity.totales.getString("limite_credito");
			grado = DetallesActivity.totales.getString("grado");
			diasCredito = DetallesActivity.totales.getString("dias");
			ruta = DetallesActivity.totales.getString("ruta");
			sucursal = DetallesActivity.totales.getString("sucursal");
			asignado = DetallesActivity.totales.getString("asignado");
			fechaEntrega = DetallesActivity.totales.getString("fecha_entrega");
			piezasTotales = DetallesActivity.totales.getString("suma_cantidad");
			piezasCredito = DetallesActivity.totales.getString("suma_credito");
			importeCredito = DetallesActivity.totales.getString("suma_productos_credito");
			piezasInversion = DetallesActivity.totales.getString("suma_inversion");
			importeInversion = DetallesActivity.totales.getString("suma_productos_inversion");
			gananciaTotal = DetallesActivity.totales.getString("suma_ganancia_cliente");


			titulo = DetallesActivity.mensajesTotalesFinal.getString("18");
			difCredito = DetallesActivity.totales.getString("diferencia_credito");
			difCreditoMessage = DetallesActivity.mensajesTotalesFinal.getString("19");
			pedidoPagoCredito = DetallesActivity.totales.getString("cantidad_pagar_cliente_credito");
			pagoCreditoMessage = DetallesActivity.mensajesTotalesFinal.getString("20");
			pagoInversionMessage = DetallesActivity.mensajesTotalesFinal.getString("21");
			pagoTotal = DetallesActivity.totales.getString("pago_al_recibir");
			pagoTotalMessage = DetallesActivity.mensajesTotalesFinal.getString("22");
			cantidadFinanciada = DetallesActivity.totales.getString("cantidad_financiar_empresa");
			cantidadFinanciadaMessage = DetallesActivity.mensajesTotalesFinal.getString("23");








			Date date = new Date();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MM-yyyy");
			date = simpleDateFormat.parse(fechaEntrega);
			fechaEntrega = simpleDateFormat1.format(date);







		} catch (Exception e) {
			e.printStackTrace();
		}






		//========disenamos nuestra etiqueta======
		//===concideramos que nuestra imrpesora es de 200dpi como dice la documentacion de PrinterComands? 200 DPI: 1 mm = 8 dots 1cm = 80dots
		//si nuestra etiqueta es de 10cm nuestro limite son 800 dots toma una etiqueta y con una regla cuadriculala de 1cm x 1cm y le sumas de 80 dots a cada cuadro
		//para que se haga mas facil dise�arla

		int r1 = 5;
		int r2 = 40;
		int r3 = 80;
		int r4 = 120;
		int r5 = 160;
		int r6 = 200;
		int r7 = 240;
		int r8 = 280;
		int r9 = 320;
		int r10 = 360;
		int r11 = 400;
		int r12 = 440;
		int r13 = 480;
		int r14 = 520;
		int r15 = 560;
		int r16 = 600;
		int r17 = 640;
		int r18 = 680;
		int r19 = 720;
		int r20 = 760;
		int r21 = 800;
		int r22 = 840;
		int r23 = 880;
		int r24 = 920;
		int r25 = 960;
		int r26 = 1000;
		int r27 = 1040;
		int r28 = 1080;
		int r29 = 1120;
		int r30 = 1160;
		int r31 = 1200;
		int r32 = 1240;
		int r33 = 1280;
		int r34 = 1320;
		int r35 = 1360;
		int r36 = 1400;
		int r37 = 1440;
		int r38 = 1480;
		int r39 = 1520;
		int r40 = 1560;

		int c1 = 5;
		int c2 = 40;
		int c3 = 80;
		int c4 = 120;
		int c5 = 160;
		int c6 = 200;
		int c7 = 240;
		int c8 = 280;
		int c9 = 320;
		int c10 = 360;
		int c11 = 400;
		int c12 = 440;
		int c13 = 480;
		int c14 = 520;
		int c15 = 560;
		int c16 = 600;
		int c17 = 640;
		int c18 = 680;
		int c19 = 720;
		int c20 = 760;
		
		

		

		mService.sendMessage(PrinterCommands.sizeMilimetros(100,200),CHAR_SET);		//definimos un tama�o de etiqueta de 10x20cm
		mService.sendMessage(PrinterCommands.gapMilimetros(2,10),CHAR_SET);
		mService.sendMessage(PrinterCommands.clearData(),CHAR_SET);


		mService.sendMessage(PrinterCommands.printText(c5,r1,2,0,2,4,empresa), CHAR_SET);

		mService.sendMessage(PrinterCommands.printText(c9,r3,2,0,2,2,"PEDIDOS"), CHAR_SET);

		mService.sendMessage(PrinterCommands.printBar(c1,r4,800,4),CHAR_SET);//si 8 dots = 1mm necesito 800 dots para 10cm


		mService.sendMessage(PrinterCommands.printText(c8,r5,1,0,3,5,fechaEntrega), CHAR_SET);

		mService.sendMessage(PrinterCommands.printText(c1,r7,1,0,3,3,ruta), CHAR_SET);
		mService.sendMessage(PrinterCommands.printText(c13,r7,1,0,3,4,sucursal), CHAR_SET);
		mService.sendMessage(PrinterCommands.reversePart(c12,r7,350,60), CHAR_SET);

		mService.sendMessage(PrinterCommands.printText(c3,r8,1,0,4,4,asignado), CHAR_SET);

		mService.sendMessage(PrinterCommands.printText(c1,r11,2,0,1,1,cuenta), CHAR_SET);
		mService.sendMessage(PrinterCommands.printText(c3,r10,2,0,2,3,nombreCliente), CHAR_SET);



		mService.sendMessage(PrinterCommands.printText(c1,r12-10,2,0,1,1,"Limi:" + limiteCredito), CHAR_SET);
		mService.sendMessage(PrinterCommands.printText(c5,r12-10,2,0,1,1,"Grad:" + grado), CHAR_SET);
		mService.sendMessage(PrinterCommands.printText(c10,r12-10,2,0,1,1,"Dias:" + diasCredito), CHAR_SET);


		mService.sendMessage(PrinterCommands.printBar(c1,r13-10,800,2),CHAR_SET);//si 8 dots = 1mm necesito 800 dots para 10cm
		mService.sendMessage(PrinterCommands.printText(c1,r13,1,0,2,2,"Contenido:"), CHAR_SET);
		imprimirDetalleProductosResumido(grado,c1,r14);
		mService.sendMessage(PrinterCommands.printBar(c1,r33,800,2),CHAR_SET);//si 8 dots = 1mm necesito 800 dots para 10cm


		mService.sendMessage(PrinterCommands.printText(c13,r34	,1,0,1,2, "Cantidad:"), CHAR_SET);
		mService.sendMessage(PrinterCommands.printText(c17,r34-20,1,0,4,4, piezasTotales), CHAR_SET);

		mService.sendMessage(PrinterCommands.printText(c13,r35,1,0,1,2, "Total Credito:"), CHAR_SET);
		mService.sendMessage(PrinterCommands.printText(c17,r35,1,0,3,3, "$"+importeCredito), CHAR_SET);

		mService.sendMessage(PrinterCommands.printText(c13,r37,1,0,1,2, "Pago al Recibir:"), CHAR_SET);
		mService.sendMessage(PrinterCommands.printText(c16,r37-20,6,0,2,3, "$"+pagoTotal), CHAR_SET);
		mService.sendMessage(PrinterCommands.reversePart(c12,r36,350,100), CHAR_SET);


		//recordar que diferencia puede venir en negativo significando que aun tiene credito disponible
		float diferencia = Float.parseFloat(difCredito);
		String temp = (diferencia<0)?"0":difCredito;
		mService.sendMessage(PrinterCommands.printText(c13,r39,1,0,1,2, "Dif credito:"), CHAR_SET);
		mService.sendMessage(PrinterCommands.printText(c16,r39,1,0,1,2, "$"+temp), CHAR_SET);

		mService.sendMessage(PrinterCommands.printText(c13,r40,1,0,1,2, "Inversion:"), CHAR_SET);
		mService.sendMessage(PrinterCommands.printText(c16,r40,1,0,1,2, "$"+ importeInversion), CHAR_SET);










		/*
		Si queremos enviar un JSON en el codigo qr debemos remplasar los caracteres de las comillas " porque la impresora no las reconoce segun la documentacion de la imrpesora
		hay que enviarlas ["], pero aun asi no funciona. entonces le vamos a enviar un ! en lugar de comillas
		pero para hacerlo facil, los peudes cambiar por simbolos comunes que saben no iran en tu cadena, para despues en el
		dispositivo que escanee el qr los vuelva a remplasar y cree el json otra vez
		String mensajeQr=DetallesActivity.totales.toString();
		String codido2 = codido1.replace("\"","*");
		Log.d("codigo",codido2);
		mService.sendMessage(PrinterCommands.printQRBarcode(440,600,"M",8,"M",0,codido2), CHAR_SET);

		//D/codigo: {!nombre!:!LUCIA ARREOLA!,!cuenta!:!8646!,!limite_credito!:!1500!,!grado!:!VENDEDORA!,!dias!:!14!,!ruta!:!HUIRAMBA!,!porcentaje_apoyo_empresa!:!1!,!porcentaje_pago_cliente!:!0!,!numero_pedido!:!1!,!fecha_entrega!:!2021-06-24!,!fecha_pago_del_credito!:!08-07-2021!,!suma_cantidad!:22,!suma_credito!:22,!suma_inversion!:0,!cantidad_sin_confirmar!:0,!suma_productos_etiqueta!:1838,!suma_productos_inversion!:0,!suma_productos_credito!:1426,!suma_ganancia_cliente!:412,!diferencia_credito!:-74,!cantidad_pagar_cliente_credito!:0,!cantidad_financiar_empresa!:1426,!pago_al_recibir!:0,!mensaje_diferencia_credito!:!Aun dispones de $74 en tu credito Kaliope!}

		 */

		/*
		No le veo caso hacer el json aqui porque al final no puedo poner un identificador como cuenta
		porque el codigo qr se haria muy largo, mejor lo voy a enviar como un texto, sirve que el codigo qr no se hace tan largo
		JSONObject codigoQr = new JSONObject();
		try {
			codigoQr.put("1",cuenta);
			codigoQr.put("2",numeroPedido);
			codigoQr.put("3",importeCredito);
			codigoQr.put("4",importeInversion);
			codigoQr.put("5",pagoTotal);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String mensajeQr=codigoQr.toString();
		String codido = mensajeQr.replace("\"","!");
		Log.d("codigo",codido);
		//D/codigo: {!1!:!8646!,!2!:!1!,!3!:!1426!,!4!:!0!,!5!:!0!}
		*/

		String codigoQr = cuenta + "," + fechaEntrega + "," + importeCredito + "," + importeInversion + "," + pagoTotal + "," + ruta;
		mService.sendMessage(PrinterCommands.printQRBarcode(c1,r34,"M",5,"M",0,codigoQr), CHAR_SET);
		mService.sendMessage(PrinterCommands.printBarcode(c1,r38,"128",80,1,0,2,2,"p"+cuenta + "," + fechaEntrega), CHAR_SET);



		//mService.sendMessage(PrinterCommands.printText(15,120,2,0,1,1,nombreCliente), CHAR_SET);


		mService.sendMessage(PrinterCommands.print(1,1),CHAR_SET);
	}



	void imprimirDetalleProductos(String grado, int xPosition, int yPosition){
		/*
		Ya que no pudimos enviar saltos de linea o retorno de carro dentor del string los imprimire por separado
		 */

		//definimos la medida inicial de cada columna
		//nos permitira mantener una imagen de tabla, y es mas facil poner las posiciones iniciales del texto desde aqui por ejemplo la columna a inicia en 10dots, la b iniciaria en 10dots + no se 80 dots, eso le dejaria
		//a la columna a 70 dots de ancho y asi sucesibamente
		int columnaA = xPosition;
		int columnaB = columnaA + 80;
		int columnaC = columnaB + 80;
		int columnaD = columnaC + 80;
		int columnaE = columnaD + 80;
		int columnaF = columnaE + 80;
		int columnaG = columnaF + 80;
		int columnaH = columnaG + 80;

		int separacionEntreFilas = 30;	//nos permite separar las filas de cada producto una distancia especifica por ejemplo la fila 1 inicia en y400, la fila 2 iniciaria en y400+40,
		int posicionDeFilaActual = yPosition;	//esta incrementara en cada bucle for de producto

		int fuente =2;
		int xMul =1;
		int yMul =1;


		try {
			//imprimimos un encabezado
			//mService.sendMessage(PrinterCommands.printText(columnaA,posicionDeFilaActual,2,0,xMul,yMul,"ID"),CHAR_SET);
			mService.sendMessage(PrinterCommands.printText(columnaA,posicionDeFilaActual,2,0,xMul,yMul,"Descr"),CHAR_SET);
			mService.sendMessage(PrinterCommands.printText(columnaB,posicionDeFilaActual,2,0,xMul,yMul,"Talla"),CHAR_SET);
			mService.sendMessage(PrinterCommands.printText(columnaC,posicionDeFilaActual,2,0,xMul,yMul,"Color"),CHAR_SET);
			mService.sendMessage(PrinterCommands.printText(columnaD,posicionDeFilaActual,2,0,xMul,yMul,"Cant"),CHAR_SET);
			mService.sendMessage(PrinterCommands.printText(columnaE,posicionDeFilaActual,2,0,xMul,yMul,"Precio"),CHAR_SET);
			mService.sendMessage(PrinterCommands.printText(columnaF,posicionDeFilaActual,2,0,xMul,yMul,"Dist"),CHAR_SET);
			mService.sendMessage(PrinterCommands.printText(columnaG,posicionDeFilaActual,2,0,xMul,yMul,"Pago"),CHAR_SET);



			//Recorremos los productos para imprimir sus detalles pero primero los de credito


			boolean hayProductosInversion = false;				//necesario para organisar los productos por credito e inversion
			for (int i=0; i < DetallesActivity.detallesPedido.length() ; i++){
				JSONObject producto = DetallesActivity.detallesPedido.getJSONObject(i);
				String idProducto = producto.getString("id_producto");
				String descripcion = producto.getString("descripcion");
				String talla = producto.getString("talla");
				String color = producto.getString("color");
				String cantidad = producto.getString("cantidad");
				String precioEtiqueta = producto.getString("precio_etiqueta");
				String precioDistribucion = "";
				String formaDePago = producto.getString("estado_producto");
				posicionDeFilaActual+=separacionEntreFilas;




				if(descripcion.length()>5){
					//si la descripcion es muy larga la cortamos para que no se ensimen los textos
					descripcion = descripcion.substring(0,5);
				}
				if(color.length()>5){
					//si la descripcion es muy larga la cortamos para que no se ensimen los textos
					color = color.substring(0,5);
				}

				//Si por algun motivo el producto llega como agotado simplemente no se imprime
				if(formaDePago.equals("CREDITO")){

					if(grado.equals("VENDEDORA")){
						precioDistribucion = producto.getString("precio_vendedora");
					}else if(grado.equals("SOCIA")){
						precioDistribucion = producto.getString("precio_socia");
					}else{
						precioDistribucion = producto.getString("precio_empresaria");
					}

					//Imprimimos cada renglon lo hacemos en varios comandos para que podamos acomodar los textos independientes y que se vea como una tabla, y no se desordenen cuando un nombre sea mayor a otro
					//solicitamos una posicion inicial para que de ahi se comiense la tabla
					//mService.sendMessage(PrinterCommands.printText(columnaA,posicionDeFilaActual,fuente,0,xMul,yMul,idProducto),CHAR_SET);
					mService.sendMessage(PrinterCommands.printText(columnaA,posicionDeFilaActual,fuente,0,xMul,yMul,descripcion),CHAR_SET);
					mService.sendMessage(PrinterCommands.printText(columnaB,posicionDeFilaActual,fuente,0,xMul,yMul,talla),CHAR_SET);
					mService.sendMessage(PrinterCommands.printText(columnaC,posicionDeFilaActual,fuente,0,xMul,yMul,color),CHAR_SET);
					mService.sendMessage(PrinterCommands.printText(columnaD,posicionDeFilaActual,1,0,xMul+1,yMul+1,cantidad),CHAR_SET);
					mService.sendMessage(PrinterCommands.printText(columnaE,posicionDeFilaActual,1,0,xMul+1,yMul+1,precioEtiqueta),CHAR_SET);
					mService.sendMessage(PrinterCommands.printText(columnaF,posicionDeFilaActual,fuente,0,xMul,yMul,precioDistribucion),CHAR_SET);
					mService.sendMessage(PrinterCommands.printText(columnaG,posicionDeFilaActual,fuente,0,xMul,yMul,formaDePago),CHAR_SET);
				}


			}
		} catch (JSONException e) {
			e.printStackTrace();
		}



	}

	/**
	 * solo imprime los precios y en dos columnas si ocupan mucho espacio. por ejemplo cuando se imprimen 22 productos
	 * solo caben 17 en la etiqueta de 10x10cm
	 * @param grado
	 * @param xPosition
	 * @param yPosition
	 */
	void imprimirDetalleProductosResumido(String grado, int xPosition, int yPosition){
		/*
		Ya que no pudimos enviar saltos de linea o retorno de carro dentor del string los imprimire por separado
		 */

		//definimos la medida inicial de cada columna
		//nos permitira mantener una imagen de tabla, y es mas facil poner las posiciones iniciales del texto desde aqui por ejemplo la columna a inicia en 10dots, la b iniciaria en 10dots + no se 80 dots, eso le dejaria
		//a la columna a 70 dots de ancho y asi sucesibamente
		int columnaA = xPosition;
		int columnaB = columnaA + 30;
		int columnaC = columnaB + 65;
		int columnaD = columnaC + 45;
		int columnaE = columnaD + 45;
		int columnaF = columnaE + 45;
		int columnaG = columnaF + 45;
		int columnaH = columnaG + 45;

		int separacionEntreFilas = 35;	//nos permite separar las filas de cada producto una distancia especifica por ejemplo la fila 1 inicia en y400, la fila 2 iniciaria en y400+40,
		int posicionDeFilaActual = yPosition;	//esta incrementara en cada bucle for de producto

		int fuente =1;
		int xMul =2;
		int yMul =2;


		try {

			//Recorremos los productos para imprimir sus detalles pero primero los de credito


			for (int i=0; i < DetallesActivity.detallesPedido.length() ; i++){
				JSONObject producto = DetallesActivity.detallesPedido.getJSONObject(i);
				String idProducto = producto.getString("id_producto");
				String descripcion = producto.getString("descripcion");
				String talla = producto.getString("talla");
				String color = producto.getString("color");
				String cantidad = producto.getString("cantidad");
				String precioEtiqueta = producto.getString("precio_etiqueta");
				String precioDistribucion = "";
				String formaDePago = producto.getString("estado_producto");





				if(descripcion.length()>5){
					//si la descripcion es muy larga la cortamos para que no se ensimen los textos
					descripcion = descripcion.substring(0,5);
				}
				if(color.length()>5){
					//si la descripcion es muy larga la cortamos para que no se ensimen los textos
					color = color.substring(0,5);
				}


				//Si por algun motivo el producto llega como agotado simplemente no se imprime


					if(grado.equals("VENDEDORA")){
						precioDistribucion = producto.getString("precio_vendedora");
					}else if(grado.equals("SOCIA")){
						precioDistribucion = producto.getString("precio_socia");
					}else{
						precioDistribucion = producto.getString("precio_empresaria");
					}

					String formaDePagoResumida = formaDePago.substring(0,3);
					//Imprimimos cada renglon lo hacemos en varios comandos para que podamos acomodar los textos independientes y que se vea como una tabla, y no se desordenen cuando un nombre sea mayor a otro
					//solicitamos una posicion inicial para que de ahi se comiense la tabla
					if(i<21){
						mService.sendMessage(PrinterCommands.printText(columnaA,posicionDeFilaActual,fuente,0,xMul,yMul,cantidad),CHAR_SET);
						mService.sendMessage(PrinterCommands.printText(columnaB,posicionDeFilaActual,fuente,0,xMul,yMul,precioEtiqueta),CHAR_SET);
						mService.sendMessage(PrinterCommands.printText(columnaC,posicionDeFilaActual,fuente,0,xMul,yMul,formaDePagoResumida),CHAR_SET);
					}else if (i<38){
						if (i==21) posicionDeFilaActual = yPosition;	//reiniciamos la altura en y solo en la primera fila
						mService.sendMessage(PrinterCommands.printText(columnaA + 200,posicionDeFilaActual,fuente,0,xMul,yMul,cantidad),CHAR_SET);
						mService.sendMessage(PrinterCommands.printText(columnaB + 200,posicionDeFilaActual,fuente,0,xMul,yMul,precioEtiqueta),CHAR_SET);
						mService.sendMessage(PrinterCommands.printText(columnaC + 200,posicionDeFilaActual,fuente,0,xMul,yMul,formaDePagoResumida),CHAR_SET);
					}else if (i<48){
						if (i==38) posicionDeFilaActual = yPosition;	//reiniciamos la altura en y solo en la primera fila
						mService.sendMessage(PrinterCommands.printText(columnaA + 360,posicionDeFilaActual,fuente,0,xMul,yMul,cantidad),CHAR_SET);
						mService.sendMessage(PrinterCommands.printText(columnaB + 360,posicionDeFilaActual,fuente,0,xMul,yMul,precioEtiqueta),CHAR_SET);
						mService.sendMessage(PrinterCommands.printText(columnaC + 360,posicionDeFilaActual,fuente,0,xMul,yMul,formaDePagoResumida),CHAR_SET);
					}

					posicionDeFilaActual+=separacionEntreFilas;




			}
		} catch (JSONException e) {
			e.printStackTrace();
		}



	}


	@Override
	public void onBackPressed() {
		//super.onBackPressed();
		/*
		new AlertDialog.Builder(activity)
				.setTitle("No puedes volver")
				.setMessage("El pedido se ha surtido.\n\nSe te enviara al menu principal")
				.setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						startActivity(new Intent(activity, MainActivity.class));
					}
				})
				.setNegativeButton("Quedarme aqui", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				}).create().show();

		 */
		Toast.makeText(activity,"No puedes volver atras, preciona el boton Menu",Toast.LENGTH_LONG).show();
	}



	/**
     * Cree una instancia de Handler para recibir el mensaje devuelto por la clase BluetoothService
     */
    private final  Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case BluetoothService.MESSAGE_STATE_CHANGE:
                switch (msg.arg1) {
                case BluetoothService.STATE_CONNECTED:   //conectado
                	Toast.makeText(getApplicationContext(), "Connect successful",
                            Toast.LENGTH_SHORT).show();
        			btnClose.setEnabled(true);
        			btnSend.setEnabled(true);
        			btnSendDraw.setEnabled(true);
                    break;
                case BluetoothService.STATE_CONNECTING:  //conectando
                	Log.d("Depuracion de Bluetooth","Conectando ...");
                    break;
                case BluetoothService.STATE_LISTEN:     //Escuche las conexiones entrantes
                case BluetoothService.STATE_NONE:
                	Log.d("Depuracion de Bluetooth","Esperando la conexion......");
                    break;
                }
                break;
            case BluetoothService.MESSAGE_CONNECTION_LOST:    //Bluetooth esta desconectado
                Toast.makeText(getApplicationContext(), "Device connection was lost",
                               Toast.LENGTH_SHORT).show();
    			btnClose.setEnabled(false);
    			btnSend.setEnabled(false);
    			btnSendDraw.setEnabled(false);
                break;
            case BluetoothService.MESSAGE_UNABLE_CONNECT:     //No se puede conectar al dispositivo
            	Toast.makeText(getApplicationContext(), "Unable to connect device",
                        Toast.LENGTH_SHORT).show();
            	break;
            }
        }
        
    };
        
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {    
        switch (requestCode) {
        case REQUEST_ENABLE_BT:      //Solicitud para activar bluetooth
            if (resultCode == Activity.RESULT_OK) {   //Bluetooth esta encendido
            	Toast.makeText(this, "Bluetooth open successful", Toast.LENGTH_LONG).show();
            } else {                 //El usuario no puede activar Bluetooth
            	finish();
            }
            break;
        case  REQUEST_CONNECT_DEVICE:     //Solicitud para conectarse a un dispositivo Bluetooth
        	if (resultCode == Activity.RESULT_OK) {   //Se ha hecho clic en un elemento de dispositivo en la lista de busqueda
                String address = data.getExtras()
                                     .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);  //Obtenga la direccion mac del dispositivo en el elemento de la lista
                con_dev = mService.getDevByMac(address);   
                
                mService.connect(con_dev);
            }
            break;
        }
    } 
    
    //Imprimir graficos
    @SuppressLint("SdCardPath")
	private void printImage() {
    	byte[] sendData = null;
    	PrintPic pg = new PrintPic();
    	pg.initCanvas(384);
    	pg.initPaint();
    	pg.drawImage(0, 0, "/mnt/sdcard/icon.jpg");
    	sendData = pg.printDraw();
    	mService.write(sendData);   //Imprimir datos de flujo de bytes
		Log.d("Depuracion de Bluetooth",""+sendData.length);
    }


	public class ConnectPaireDev extends Thread {
		public void run(){
			while(true)                      //Asegurese de que Bluetooth este completamente encendido
				if( mService.isBTopen() == true)
					break;
			//La aplicacion se inicia y atraviesa los dispositivos conectados.
			Set<BluetoothDevice> pairedDevices = mService.getPairedDev();
			if (pairedDevices.size() > 0) {
				for (BluetoothDevice device : pairedDevices) {
					if(conn_flag == 1){
						conn_flag = 0;
						break;
					}
					while(true)
						if(conn_flag==-1 || conn_flag==0)  //Espere a que se complete la operacion de conexion del ultimo ciclo
							break;
					mService.connect(device);
					conn_flag = 2;
				}
			}
		}
	}
}
