package com.davidcompany.almacenpedidos;

import android.util.Log;

import androidx.annotation.Size;

import org.jetbrains.annotations.NotNull;

import java.util.zip.DeflaterInputStream;

public class PrinterCommands {

    //eSTOS COMANDOS SE CONSIGUEN EN LA PAGINA OFICIAL DE LA IMPRESORA
    //http://www.zjiang.com/en/init.php/service/manual
    //SELECCIONAS LA IMPRESORA DESPUES EN SERVICE SUPORT ENCUENTRAS EL SDK EN PRINTER DRIVER
    //Y ENCUENTRAS EN PRINTER MANUAL EL ARCHIVO PRINTER PROGRAMMER MANUALS Y ES UN PDF CON LOS COMANDOS Y LO QUE HACEN
    public static final byte HT = 0x9;
    public static final byte LF = 0x0A;
    public static final byte CR = 0x0D;
    public static final byte ESC = 0x1B;
    public static final byte DLE = 0x10;
    public static final byte GS = 0x1D;
    public static final byte FS = 0x1C;
    public static final byte STX = 0x02;
    public static final byte US = 0x1F;
    public static final byte CAN = 0x18;
    public static final byte CLR = 0x0C;
    public static final byte EOT = 0x04;
    public static final byte MX = 0x24;

    public static final byte[] INIT = {27, 64};
    public static byte[] FEED_LINE = {10};

    /*public static byte[] SELECT_FONT_A = {27, 77, 0};
    public static byte[] SELECT_FONT_B = {0x1B,0x21,0x00};
    public static byte[] SELECT_FONT_C = {27, 77, 48};
    public static byte[] SELECT_FONT_D = {27, 77, 49};*/


    public static byte[] SELECT_FONT_A = {ESC,0x21,0x00};
    public static byte[] SELECT_FONT_B = {ESC,0x21,0x01};
    public static byte[] SELECT_FONT_C = {ESC,0x21,0x10};

    public static byte[] SELECT_FONT_D = {GS,0x21,0x10};
    public static byte[] SELECT_FONT_E = {GS,0x21,0x20};
    public static byte[] SELECT_FONT_F = {GS,0x21,0x21};



    public static byte[] SET_BAR_CODE_HEIGHT = {29, 104, 100};
    public static byte[] PRINT_BAR_CODE_1 = {29, 107, 2};
    public static byte[] SEND_NULL_BYTE = {0x00};

    public static byte[] SELECT_PRINT_SHEET = {0x1B, 0x63, 0x30, 0x02};
    public static byte[] FEED_PAPER_AND_CUT = {0x1D, 0x56, 66, 0x00};

    public static byte[] SELECT_CYRILLIC_CHARACTER_CODE_TABLE = {0x1B, 0x74, 0x11};

    public static byte[] SELECT_BIT_IMAGE_MODE = {0x1B, 0x2A, 33, -128, 0};
    public static byte[] SET_LINE_SPACING_24 = {0x1B, 0x33, 24};
    public static byte[] SET_LINE_SPACING_30 = {0x1B, 0x33, 30};

    public static byte[] TRANSMIT_DLE_PRINTER_STATUS = {0x10, 0x04, 0x01};
    public static byte[] TRANSMIT_DLE_OFFLINE_PRINTER_STATUS = {0x10, 0x04, 0x02};
    public static byte[] TRANSMIT_DLE_ERROR_STATUS = {0x10, 0x04, 0x03};
    public static byte[] TRANSMIT_DLE_ROLL_PAPER_SENSOR_STATUS = {0x10, 0x04, 0x04};

    public static final byte[] ESC_FONT_COLOR_DEFAULT = new byte[] { 0x1B, 'r',0x00 };
    public static final byte[] FS_FONT_ALIGN = new byte[] { 0x1C, 0x21, 1, 0x1B, 0x21, 1 };
    public static final byte[] ESC_ALIGN_LEFT = new byte[] { 0x1b, 'a', 0x00 };
    public static final byte[] ESC_ALIGN_RIGHT = new byte[] { 0x1b, 'a', 0x02 };
    public static final byte[] ESC_ALIGN_CENTER = new byte[] { 0x1b, 'a', 0x01 };
    public static final byte[] ESC_SETTING_BOLD = new byte[]{ESC, 0x45, 1};
    public static final byte[] ESC_CANCEL_BOLD = new byte[] { 0x1B, 0x45, 0 };


    /*********************************************/
    public static final byte[] ESC_HORIZONTAL_CENTERS = new byte[] { 0x1B, 0x44, 20, 28, 00};
    public static final byte[] ESC_CANCLE_HORIZONTAL_CENTERS = new byte[] { 0x1B, 0x44, 00 };
    /*********************************************/

    public static final byte[] ESC_ENTER = new byte[] { 0x1B, 0x4A, 0x40 };
    public static final byte[] PRINTE_TEST = new byte[] { 0x1D, 0x28, 0x41 };


    /**
     * cOMANDOS PARA LA IMPRESORA DE ETIQUETAS ADESIVAS ZJING
     * LOS HAGO LUISDA IMPRESORA ZJ-9200
     * http://www.zjiang.com/en/init.php/product/main?id=26
     * http://www.zjiang.com/en/init.php/service/manual
     * DESCARGA EL PDF LLAMADO PRINTER PROGRAMMER MANUAL
     * AGREGE EL PDF A LA CARPETA DEL PROYECTO =D
     * SOLO HAY QUE ENVIAR COMO STRING EL COMANDO DE TEXTO EN ESTE CASO ESTA IMPRESORA USA TEXTO
     * Y LA DE CODIGO DE BARRAS USABA BYTES
     *  POR EJEMPLO PARA ENVIAR UN SELFTEXT TIENES QUE ENVIAR
     *  String msg = "SELFTEST";
     *
     *  Y LUEGO SE LO ESCRIBES AL BLUETOOT
     *  mService.sendMessage(msg+"\n", "GBK");
     * 	//mService.write(PrinterCommands.PRINTE_TEST);
     *
     */

    public static final String CHAR_SET = "GBK";//no se porque se le tiene que enviar este string al metodo sendMessaje de la impresora

    /**
     * Define the width and height of the label paper
     *Grammar ：
     *
     * imperial
     * SIZE x,y
     *
     * metric system
     * (mm)
     * SIZE x mm, y mm
     * Parameter Explain
     * x Define the width of the label paper. (inch or mm)
     * y D e f i n e t h e w i d tho f t h e l a b e l p a p e r . (inch or mm)
     * Notes:
     * 200 DPI: 1 mm = 8 dots
     * 300 DPI: 1mm = 12 dots
     * Under the metric system., the space is required between x a y d “mm” . Example：
     * (1) i m p e r i a l s y s t e m (inch)
     * SIZE 3.5, 3.00
     *
     * (2) metric system
     * (mm)
     * SIZE 100 mm, 100 mm
     *
     *
     *
     * @param x mm
     * @param y mm
     * @return comando
     */
    @NotNull
    public static String sizeMilimetros(int x, int y) {
        String comando = "SIZE %1d mm,%2d mm"+ "\n";

       String temp = String.format(comando,x,y);
        Log.d("comando", temp);
        return temp;
    }
    /**
     * Define the width and height of the label paper
     *Grammar ：
     *
     * imperial
     * SIZE x,y
     *
     * metric system
     * (mm)
     * SIZE x mm, y mm
     * Parameter Explain
     * x Define the width of the label paper. (inch or mm)
     * y D e f i n e t h e w i d tho f t h e l a b e l p a p e r . (inch or mm)
     * Notes:
     * 200 DPI: 1 mm = 8 dots
     * 300 DPI: 1mm = 12 dots
     * Under the metric system., the space is required between x a y d “mm” . Example：
     * (1) i m p e r i a l s y s t e m (inch)
     * SIZE 3.5, 3.00
     *
     * (2) metric system
     * (mm)
     * SIZE 100 mm, 100 mm
     *
     *
     *
     * @param x mm
     * @param y mm
     * @return comando
     */
    @NotNull
    public static String sizePulgadas(int x, int y) {
        String comando = "SIZE %1d,%2d"+ "\n";

        String temp = String.format(comando,x,y);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * Grammar ：
     * (1) imperial system (inch)-> GAP m,n
     * (2) metric system(mm)-> GAP m mm, n mm
     *
     * Parameter Explain
     * m Define label gap height (inch or mm) 0 ≤ m ≤ 1 (inch), 0 ≤ m ≤ 25.4 (mm)
     * n Define the compensation value of the gap height of the label (inches or
     * mm) n ≤ label length (inch or mm)
     * 0,0 Continuous paper mode
     * Notes:
     * In the metric system, there must be a space between the parameters “m” and “mm”. Example: Label
     * Paper
     * (1) E n g l i s h S y s t e m (inch)
     * GAP 0.12,0
     * (2) Metric System (mm)
     * GAP 3 mm,0
     * (3) C o n t i n u o u s
     * p a p e r m o d e
     * GAP 0,0
     * @param gap
     * @param compensacion
     * @return
     */
    @NotNull
    public static String gapMilimetros(int gap, int compensacion) {
        String comando = "GAP %1d mm,%2d mm"+ "\n";

        String temp = String.format(comando,gap,compensacion);
        Log.d("comando", temp);
        return temp;
    }
    /**
     * Grammar ：
     * (1) imperial system (inch)-> GAP m,n
     * (2) metric system(mm)-> GAP m mm, n mm
     *
     * Parameter Explain
     * m Define label gap height (inch or mm) 0 ≤ m ≤ 1 (inch), 0 ≤ m ≤ 25.4 (mm)
     * n Define the compensation value of the gap height of the label (inches or
     * mm) n ≤ label length (inch or mm)
     * 0,0 Continuous paper mode
     * Notes:
     * In the metric system, there must be a space between the parameters “m” and “mm”. Example: Label
     * Paper
     * (1) E n g l i s h S y s t e m (inch)
     * GAP 0.12,0
     * (2) Metric System (mm)
     * GAP 3 mm,0
     * (3) C o n t i n u o u s
     * p a p e r m o d e
     * GAP 0,0
     * @param gap
     * @param compensacion
     * @return
     */
    @NotNull
    public static String gapPulgadas(int gap, int compensacion) {
        String comando = "GAP %1d,%2d"+ "\n";

        String temp = String.format(comando,gap,compensacion);
        Log.d("comando", temp);
        return temp;
    }

    /**
     *Defina la longitud adicional de la etiqueta posterior
     * a la impresión, especialmente cuando use la función
     * de desbloqueo o cortador automático, para ajustar la
     * posición del tope de etiquetas, la impresora empujará más o menos.
     * Grammar：
     * EnglishSystem(Inch)OFFSETm
     * metricsystem(mm)OFFSETm mm
     *
     * example
     * EnglishSystem(Inch)  OFFSET0.5
     * metricsystem(mm)     OFFSET12.7 mm
     * @param distancia
     * @return
     */
    @NotNull
    public static String offsetMilimetros(int distancia) {
        String comando = "OFFSET %1d mm"+ "\n";

        String temp = String.format(comando, distancia);
        Log.d("comando", temp);
        return temp;
    }


    /**
     * Function ： Set the printer's printing speed.
     * Grammar：SPEED n
     * Parameter Explain
     * n The printing speed per second , measured in inches.
     * Example：
     * SPEED 4
     * @param speed
     * @return
     */
    @NotNull
    public static String speed(int speed) {
        String comando = "SPEED %1d"+ "\n";

        String temp = String.format(comando, speed);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * Function ： Set the printer's printing concentration.
     * Grammar：DENSITY n
     * Parameter Explain
     * n 1~7
     * 1 , The lowest concentration.
     * 7 , The deepest concentration.
     * Example：
     * DENSITY 3
     * @param density
     * @return
     */
    @NotNull
    public static String density(int density) {
        String comando = "DENSITY %1d"+ "\n";

        String temp = String.format(comando, density);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * Function ： Set the printing direction ， T h i s
     * s e t t i n g w i l l b e r e c o r d e d i n
     * EEPROM
     * Grammar：DIRECTION n
     * Parameter Explain
     * n 0 o r 1
     * P l e a s e r e f e r t o t h e b e l o w e x a m p l e s :
     *
     * revisar el pdf pero basicamente puedes elegir si el texto se imprime hacia la direccion que
     * se mueve el papel, o se imprime al lado contrario. no creo que sea muy importante esta configuracion
     * @param direccion 0 o 1
     * @return
     */
    @NotNull
    public static String direction(int direccion) {
        String comando = "DIRECTION %1d"+ "\n";

        String temp = String.format(comando, direccion);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * Función: Defina las coordenadas del punto de referencia en el papel de etiqueta en relación con el origen.<p>
     * Consulte la siguiente ilustración:<p></p>
     *
     * se invierte si usas directio 1 o 0<p></p>
     * Grammar：
     * REFERENCE x, y<p></p>
     * Parameter Explain
     * x horizontal corrdinates, unit “dot”<p>
     * y vertical coordinate，unit “dot”<p></p><p></p>
     * Notes： 200 DPI: 1 mm = 8 dots
     * 300 DPI: 1 mm = 12 dots<p></p><p></p>
     * Example：
     * REFERENCE 10,10<p>
     * Other reference items：<p>
     * DIRECTION<p>
     * @param x posicion en x dots
     * @param y posicion en y dots
     * @return
     */
    @NotNull
    public static String reference(int x, int y) {
        String comando = "REFERENCE %1d,%2d"+ "\n";

        String temp = String.format(comando,x,y);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * Function: This instruction is used to select
     * the corresponding international
     * character set.
     * Grammar：CODEPAGE n
     * Parameter Explain
     * n Code page number
     * Example：
     * CODEPAGE 0
     * @param codePage
     * @return
     */
    @NotNull
    public static String codePage(int codePage) {
        String comando = "CODEPAGE %1d"+ "\n";

        String temp = String.format(comando,codePage);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * Function ： Clear data me imagino que borra las configuraciones del tamaño de etiqueta offset etc que se definieron primero
     * cache
     * Grammar：CLS
     * Noted：This instruction must be placed after the SIZE instruction.
     * Example：
     * CLS
     * Other reference items：
     * SIZE, GAP
     * @return
     */
    @NotNull
    public static String clearData() {
        String comando = "CLS"+ "\n";

        Log.d("comando", comando);
        return comando;
    }

    /**
     * Function：Empuje el papel de etiquetas hacia adelante a la longitud especificada.
     * Grammar：FEED n
     * Parameter Explain
     * n Unit：dot
     * 1 ≤ n ≤ 9999
     * Example：
     * FEED 40
     * Noted：200 DPI: 1 mm = 8 dots
     * 300 DPI: 1 mm = 12 dots
     * Other reference items：
     * BACKFEED, SIZE, GAP, HOME, FORMFEED
     * @param distanceInDots
     * @return
     */
    @NotNull
    public static String feed(int distanceInDots) {
        String comando = "FEED %1d"+ "\n";

        String temp = String.format(comando,distanceInDots);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * Function ：Tire del papel de etiquetas hacia atrás la longitud especificada.
     * Grammar：BACKFEED n
     * Parameter Explain
     * n Unit：dot
     * 1 ≤ n ≤ 9999
     * Warning ： Improper backhaul can cause the
     * phenomenon of "paper jam".
     * Notes：
     * 200 DPI: 1 mm = 8 dots
     * 300 DPI: 1 mm = 12 dots
     * Example：
     * BACKFEED 40
     * BACKUP 40
     * @param distanceInDots
     * @return
     */
    @NotNull
    public static String backFeed(int distanceInDots) {
        String comando = "BACKFEED %1d"+ "\n";

        String temp = String.format(comando,distanceInDots);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * Function ： Empuje la etiqueta hacia adelante hasta el inicio posición de la siguiente etiqueta.
     * creo que esta funcion usa el sensor que identifica cuando la etiqueta se termina y comienza la sigiente
     * Grammar：FORMFEED
     * Example：
     * SIZE 50 mm,40 mm
     * GAP 0 mm,0 mm
     * SPEED 4
     * DENSITY 3
     * DIRECTION 0
     * OFFSET 0 mm
     * REFERENCE 0 mm,0 mm
     * SET PEEL OFF
     * SET COUNTER @0 +1
     * @0="000001"
     * FORMFEED
     * CLS
     * BOX 1,1,360,65,12
     * TEXT 25,25,"3",0,1,1,"FORMFEED COMMAND TEST"
     * TEXT 25,80,"3",0,1,1,@0
     * PRINT 3,1
     * @return
     */
    @NotNull
    public static String formFeed() {
        String comando = "FORMFEED"+ "\n";

        Log.d("comando", comando);
        return comando;
    }

    /**
     * Función: cuando utilice etiquetas que contengan espacios o marcas negras, si no está seguro
     * si la primera etiqueta está en la posición de impresión correcta, este comando puede presionar
     * la etiqueta avanza hasta el comienzo de la siguiente etiqueta para comenzar a imprimir.
     * Grammar：HOME
     * Example：
     * SIZE 50 mm, 40 mm
     * GAP 2 mm,0 mm
     * SPEED 4
     * DENSITY 7
     * DIRECTION 0
     * OFFSET 0 mm
     * REFERENCE 0,0
     * SET PEEL OFF
     * SET COUNTER @0 +1
     * @0="000001"
     * HOME
     * CLS
     * BOX 1,1,360,65,12
     * TEXT 25,25,"3",0,1,1,"HOME COMMAND TEST"
     * TEXT 25,80,"3",0,1,1,@0
     * PRINT 3,1
     * Other reference
     * @return
     */
    @NotNull
    public static String home() {
        String comando = "HOME"+ "\n";

        Log.d("comando", comando);
        return comando;
    }

    /**
     * Function ： Imprime la etiqueta que esta almacenado en la caché de datos.
     * me imagino que en la impresora con forme le envias alguna etiqueta la almacena en su cache
     * y con el numero de etiqueta puedes elegir cual de ellas se imprimira y cuantas copias
     * porque para imprimir el texto que justo acabas de enviar tienes que poner PRINT 1,1
     * Grammar：PRINT m [,n]
     * Parameter Explain
     * m Print Numbers
     * 1 ≤ m ≤ 999999999
     * N The number of copies of each label should be repeated.
     * 1 ≤ n ≤ 999999999
     * Example：
     * SIZE 60 mm, 40 mm
     * SET COUNTER @1 1
     * @1=”000”
     * CLS
     * TEXT 10,10,”3”,0,1,1,@1
     * PRINT 3,2
     * @param numeroImpresion
     * @param numeroCopias
     * @return
     */
    @NotNull
    public static String print(int numeroImpresion, int numeroCopias) {
        String comando = "PRINT %1d,%2d"+ "\n";

        String temp = String.format(comando,numeroImpresion,numeroCopias);
        Log.d("comando", temp);
        return temp;
    }



    /**
     * SELFTEST
     * Function ： Print information directly on the label paper without self-testing。
     * Grammar：
     * SELFTEST
     * Example：
     * SELFTEST
     * @return
     */
    @NotNull
    public static String selftest() {
        String comando = "SELFTEST"+ "\n";

        Log.d("comando", comando);
        return comando;
    }

    /**
     * Function: Dibuja líneas o dibuja largas tiras.
     * Necesitas decir en que cordenada quieres que se imprima la linea que tan larga y que tan anchaa
     * Grammar: BAR x,y,width,height
     * Parameter Explain
     * x T o p l e f t c o r n e r X coordinate，unit: dot
     * y T o p l e f t c o r n e r Y coordinate，unit: dot
     * width Line width，unit: dot
     * height Line height，unit: dot
     * Noted：200 DPI: 1 mm = 8 dots
     * 300 DPI: 1 mm = 12 dots
     * Example：
     * SIZE 4,2.5
     * GAP 0,0
     * SPEED 6
     * DENSITY 3
     * DIRECTION 0
     * CLS
     * BAR 100, 100, 300, 200
     * PRINT 1,1
     * @param xPosition
     * @param xPosition
     * @param widthDots
     * @param heigntDots
     * @return
     */
    @NotNull
    public static String printBar(int xPosition, int yPosition, int widthDots, int heigntDots) {
        String comando = "BAR %1d,%2d,%3d,%4d"+ "\n";

        String temp = String.format(comando,xPosition,yPosition,widthDots,heigntDots);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * BARCODE
     * Function：Print one dimensional bar code，<p>
     * Below is a list of supported barcode:<p>
     *  Code 128<p>
     *  Code 39<p>
     *  Code 93<p>
     *  EAN 13<p>
     *  EAN 8<p>
     *  Coda bar<p>
     *  UPC-A<p>
     *  UPC-E<p>
     *  ITF<p>
     * Grammar：<p>
     * BARCODE X,Y,”code type”,height,human readable,rotation,narrow,wide,“code”<p>
     * Parameter Explain<p>
     * X Defined barcode top left corner X coordinate<p>
     * Y Defined barcode top left corner X coordinate<p>
     *
     * Barcode type:<p><p>
     *
     * height Barcode height (dot)<p><p>
     *     Noted： 200 DPI: 1 mm = 8 dots<p>
     *      * 300 DPI: 1 mm = 12 dots<p><p>
     *
     * human readable 0 : No human eye can identify the code.<p>
     * 1:Human eye can identify the code.<p><p>
     *
     * rotation Rotate the bar code Angle clockwise.<p>
     * 0 non-rotation<p>
     * 90 Rotate 90 degrees clockwise<p>
     * 180 Rotate 180 degrees clockwise<p>
     * 270 Rotate 270 degrees clockwise<p><p>
     *
     * narrow Narrow bar code scaling (dot)<p><p>
     *
     * wide Wide bar code scaling (dot)<p><p>
     * Example：<p>
     * BARCODE 100,100,”39”,96,1,0,2,4,”1000”<p>
     * BARCODE 10,10,”128”,48,1,0,2,2,”!123456799!”<p>
     * @param xPositionDots posicion inicial en x dots
     * @param yPositionDots posicion inicial en x dots
     * @param barCode       codigo del barcode en string
     * @param heigntDots    la altura del codigo de barras
     * @param humanRedable  0 el ojo humano no puede identificar el codigo 1 el ojo humano puede identificarlo
     * @param rotation      rotacion 0, 90 180 o 270
     * @param narrowDots    La escala de lo estrecho del codigo
     * @param wideDots      El ancho del codigo de barras
     * @param contenido     el contenido del codigo en string
     * @return
     */
    @NotNull
    public static String printBarcode(int xPositionDots, int yPositionDots, String barCode, int heigntDots,
                                      int humanRedable, int rotation, int narrowDots,int wideDots, String contenido) {
        String comando = "BARCODE %d,%d,\"%s\",%d,%d,%d,%d,%d,\"%s\""+ "\n";

        String temp = String.format(comando,xPositionDots,yPositionDots,barCode,heigntDots,humanRedable,rotation,narrowDots,wideDots,contenido);
        Log.d("comando", temp);
        return temp;
    }



    public static void printBitMap(com.zj.btsdk.BluetoothService bluetoothService, int xPositionDots, int yPositionDots, int xWidth, int yHeight, int mode, byte[] bitMap) {
        String comando = "BITMAP %d,%d,%d,%d,%d,";


        String bitmapString = bitMap.toString();
        Log.d("BitmapString", bitmapString);
        Log.d("BitmapLeght", String.valueOf(bitMap.length));

        String temp = String.format(comando,xPositionDots,yPositionDots,xWidth,yHeight,mode);

        byte [] com = temp.getBytes();
        Log.d("comando", temp);
        bluetoothService.write(com);
        bluetoothService.write(bitMap);
    }


    /**
     * Puedes imprimir un rectangulo o cuadro
     *Function：Draw EspBox
     * Grammar：BOX X_start, Y_start, X_end, Y_end, line thickness
     * Parameter Explain
     * X_start Box top left X coordinate，unit：dot
     * Y_start Box top left Y coordinate，unit：dot
     * X_end Box bottom right X coordinate，unit：dot
     * Y_end Box bottom right Y coordinate，unit：dot
     * line thickness Box line thickness，unit：dot
     * Noted：200 DPI: 1 mm = 8 dots
     * 300 DPI: 1 mm = 12 dots
     * Example：
     * SIZE 60 mm,40 mm
     * GAP 0,0
     * SPEED 6
     * DENSITY 3
     * DIRECTION 0
     * CLS
     * BOX 100,100,200,200,5
     * PRINT 1,1
     * @param xPositionDots Box top left X coordinate，unit：dot
     * @param yPositionDots Box top left Y coordinate，unit：dot
     * @param xEndPositionDots Box bottom right X coordinate，unit：dot
     * @param yEndPositionDots Box bottom right Y coordinate，unit：dot
     * @param lineThicknessDots Box line thickness，unit：dot
     * @return
     */
    @NotNull
    public static String printBox(int xPositionDots, int yPositionDots,int xEndPositionDots, int yEndPositionDots, int lineThicknessDots) {
        String comando = "BOX %d,%d,%d,%d,%d"+ "\n";

        String temp = String.format(comando,xPositionDots,yPositionDots,xEndPositionDots,yEndPositionDots,lineThicknessDots);
        Log.d("comando", temp);
        return temp;
    }

    /**
     *Para entender esto remarquemos lo siguiente, Hemos dicho que la impresora va acomulando en la cache
     * los comandos que le enviamso, si le enviamos el codigo de barras en la posicion 10,10 ahi lo guarda
     * despues le enviamso un texto debajo de ese codigo de barras en la posicion 10,100
     * entonces la impresora lo guarda en su cache, creando como "su imagen de la etiqueta" aunque claro no nececsita
     * una imagen simpelmente va acumulando sus comandos. esto no lo imprime hasta que se envia el comando PRINT
     *
     * entonces esa "imagen" del codigo de barras y el texto esta creada en una posicion de su cache entonces nosotros
     * con este comando podemos colocarnos en cualquier punto de esa imagen y borrar una area especifica por ejemplo
     * si le queremos borrar un pedasito al texto o al codigo de barras supongamos que el codigo de barras va desde
     * la el punto 10 en x hasta el punto 100 en x de ancho por una altura de 100 puntos (y= 10,100)entonces nos podemos colocar
     * en x40,y40 y darle un ancho de 10 dots y alto de 20 dots entonces al codigo de barras casi a la mitad se le haria un cuadro en
     * blanco de 10 dots de ancho
     *
     *ERASE
     * Function: Clears the area specified in the image
     * buffer.
     * Grammar ： ERASE X_start, Y_start, X_width,
     * Y_height
     * Parameter Explain
     * X_start To clear the top left corner of the area X coordinates，unit：dot
     * Y_start To clear the top left corner of the area Y coordinates，unit：dot
     * X_width To clear the width of the area，unit：dot
     * Y_height To clear the height of the area，unit：dot
     * Example：
     * SIZE 60 mm,60 mm
     * GAP 0,0
     * SPEED 6
     * DENSITY 3
     * DIRECTION 0
     * CLS
     * BAR 100, 100, 300, 300
     * ERASE 150,150,200,200
     * PRINT 1,1
     *
     * @param xStartPositionDots To clear the top left corner of the area X coordinates，unit：dot
     * @param yStartPositionDots To clear the top left corner of the area Y coordinates，unit：dot
     * @param xWidh To clear the width of the area，unit：dot
     * @param yHeight To clear the height of the area，unit：dot
     * @return
     */
    public static String erasePart(int xStartPositionDots, int yStartPositionDots,int xWidh, int yHeight) {
        String comando = "ERASE %d,%d,%d,%d"+ "\n";

        String temp = String.format(comando,xStartPositionDots,yStartPositionDots,xWidh,yHeight);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * Al pedir solo el filename y no un bytearray como lo haciamos apra imprimir el logo de kaliope
     * quiero imaginar que el file name hace referencia a un archivo almacenado en la propia memoria de lla
     * impresora, no en la memoria del telefono. me imagino que o este metodo le envia el BMP a la memoria
     * para guardarse por primera vez, oo hay otro comando que lo guarda y este solo lo rescata
     *
     * Function：Print BMP format file
     * Grammar：PUTBMP X,Y,”filename”
     * Parameter Explain
     * X BMP graphic upper left corner X coordinate
     * Y BMP graphic upper left corner Y coordinate
     * filename BMP graphic what uploaded to the printer.
     * Noted ： recommended to use BMP image files with only
     * black and white colors.
     * @param xStartPositionDots BMP graphic upper left corner X coordinate
     * @param yStartPositionDots BMP graphic upper left corner Y coordinate
     * @param filename BMP graphic what uploaded to the printer.
     * @return
     */
    public static String putBMP(int xStartPositionDots, int yStartPositionDots,String filename) {
        String comando = "PUTBMP %d,%d,\"%s\""+ "\n";

        String temp = String.format(comando,xStartPositionDots,yStartPositionDots,filename);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * no se que formato es el PCX jeje pero ahi esta el comando
     * Function：Print PCX format file.
     * Grammar：PUTPCX X,Y,”filename”
     * Parameter Explain
     * X PCX graphic top-left corner X coordinate
     * Y PCX graphic top-left corner Y coordinate
     * filename PCX graphic what uploaded to the printer.
     * @param xStartPositionDots PCX graphic top-left corner X coordinate
     * @param yStartPositionDots PCX graphic top-left corner Y coordinate
     * @param filename PCX graphic what uploaded to the printer.
     * @return
     */
    public static String putPCX(int xStartPositionDots, int yStartPositionDots,String filename) {
        String comando = "PUTPCX %d,%d,\"%s\""+ "\n";

        String temp = String.format(comando,xStartPositionDots,yStartPositionDots,filename);
        Log.d("comando", temp);
        return temp;
    }

    /**
     *La impresora tiene la capacidad de ella generar solita el codigo QR solo ajustando los parametros
     * y enviando el string con el contenido, obiament podriamos hacerlo tambien con la libreria zsing
     * para crear los codigos qr desde el movil, y enviarselos como imagen a la imrpesora, en este caso
     * la impresora nos ahorro un poco de trabajo.<p></p>
     *
     * Grammar：<p>
     * QRCODE X, Y, ECC Level, cell width, mode, rotation, [model, mask,]"Data string”<p></p>
     * Parameter Explain<p>
     * X QRCODE bar code top left X coordinates.<p>
     * Y QRCODE bar code top left Y coordinates.<p></p>
     *
     * ECC level Error correction level.
     * este es el nivel capacidad que tiene el codigo qr de recuperar datos dañados cuando el codigo
     * esta sucio roto etc, el provblema con usar el H o Q es que tipicamente el codigo QR se vuelve
     * enorme porque se repiten muchos datos, recomiendan usarlo para casos donde el codigo qr esta
     * expuesto a la suciedad. Tipicamente deberia usarse M, y la L solo para casos donde el codigo QR estara
     * muy concervado cuidado.<p>
     * L 7%<p>
     * M 15%<p>
     * Q 25%<p>
     * H 30%<p>
     * cell width 1~10 lo ancho del qr define basicamente el tamaño<p></p>
     *
     * mode Automatically generated code/manually generated code<p>
     * A Auto<p>
     * M Manual<p></p>
     *
     * rotation Rotate<p>
     * clockwise<p>
     * 0 non-rotation<p>
     * 90 Rotate 90 degrees clockwise<p>
     * 180 Rotate 180 degrees clockwise<p>
     * 270 Rotate 270 degrees clockwise<p>
     *
     *     Esta parte del model barcode y la mascara no la entiendo aun
     *     pero en los ejemplos de abajo solo mandan String, al aprecer
     *     esto del model y la mascara se tienen que colocar entre corchetes, segun la gramatica
     *     pero realmente no termino de comprenderlo. Por lo visto con los ejemplos no pasa nada
     *     si solo se manda el string.<p></p>
     * model Barcode generation style.<p>
     * 1 (Preset), original version<p>
     * 2 Expanded version <p>
     * mask Range：0~8，Preset 7 <p></p>
     *
     * Data string Barcode information content.<p>
     * Available coded character set：<p>
     * 1). numerical data：number 0~9
     * 2). Alphanumeric data： number 0~9； capital letter A-Z; O t h e r ： space，$%*+-
     * ./,GB18030 character set:
     * List：<p></p>
     * SIZE 60 mm,60 mm<p>
     * CAP 0,0<p>
     * CLS<p>
     * QRCODE 10,10,H,4,M,0,"AABC!B0005\["]abc\["]!N123"<p>
     * QRCODE 310,310,H,4,M,0,"B0001\["]!K 打印机!B0010\["]ABCabc123"<p>
     * PRINT 1,1<p>
     * <p>
     *     <p>
     *
     *
     * @param xPositionDots QRCODE bar code top left X coordinates.
     * @param yPositionDots QRCODE bar code top left Y coordinates.
     * @param eccLevel Error correction level. <p> L=7% <p> M=15% <p> Q=25% <p> H=30% <p><p>
     * @param cellWidth 1~10 <p>
     * @param mode Automatically generated code/manually generated code A=Auto <p> M=Manual<p><p>
     * @param rotation Rotation <p> 0 non-rotaation 90 <p> 180 <p> 270
     * @param contenido el contenido del qr si quieres que se impriman comillas ponerlas entre ["]
     * @return
     */
    @NotNull
    public static String printQRBarcode(int xPositionDots, int yPositionDots, String eccLevel, int cellWidth,
                                      String mode, int rotation, String contenido) {
        String comando = "QRCODE %d,%d,%s,%d,%s,%d,\"%s\""+ "\n";

        String temp = String.format(comando,xPositionDots,yPositionDots,eccLevel,cellWidth,mode,rotation,contenido);
        Log.d("comando", temp);
        return temp;
    }

    /**
     *Function：Invert the specified area in the image cache
     * Lo que hace es si tienes la palabra HOLA colocas el reverse en esa area donde se escribio HOLA
     * y la impresora invierte el colo en ese cuadro es decir se imprimira un cuadro negro y las letras
     * blancas dentro. en lugar de que solo se escriban las palabras HOLA en negro
     * Grammar ： REVERSE X_start, Y_start, X_width,Y_height<p>
     * Parameter Explain<p>
     * X_start To clear the top left corner of the area X coordinates，unit：dot<p>
     * Y_start To clear the top left corner of the area Y coordinates，unit：dot<p>
     * X_width To clear the width of the area，unit：dot<p>
     * Y_height To clear the height of the area，unit：dot<p>
     * Noted： 200 DPI: 1 mm = 8 dots<p>
     * 300 DPI: 1 mm = 12 dots<p>
     * Example：<p>
     * SIZE 4,2.5<p>
     * GAP 0,0<p>
     * SPEED 6<p>
     * DENSITY 3<p>
     * DIRECTION 0<p>
     * CLS<p>
     * TEXT 100,100,"3",0,1,1,"REVERSE"<p>
     * REVERSE 90,90,128,40<p>
     * PRINT 1,1<p>
     *
     * @param xStartPositionDots To clear the top left corner of the area X coordinates，unit：dot
     * @param yStartPositionDots To clear the top left corner of the area Y coordinates，unit：dot
     * @param xWidh To clear the width of the area，unit：dot
     * @param yHeight To clear the height of the area，unit：dot
     * @return
     */
    public static String reversePart(int xStartPositionDots, int yStartPositionDots,int xWidh, int yHeight) {
        String comando = "REVERSE %d,%d,%d,%d"+ "\n";

        String temp = String.format(comando,xStartPositionDots,yStartPositionDots,xWidh,yHeight);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * Function：print text
     * Grammar ： TEXT X,Y,”font”,rotation,x-multiplication,y-multiplication,“content”<p></p>
     * Parameter Explain<p>
     * X Text box upper left corner X coordinate<p>
     * Y Text box upper left corner Y coordinate<p>
     *
     * font Font name<p>
     * 1 8 x 12 English numerals<p>
     * 2 12 x 20 English numerals<p>
     * 3 16 x 24 English numerals<p>
     * 4 24 x 32 English numerals<p>
     * 5 32 x 48 English numerals<p>
     * 6 14 x 19 English numerals OCR-B<p>
     * 7 14 x 25 English numerals OCR-A<p>
     * 8 21 x 27 English numerals OCR-B<p>
     * 9 9 x 17 English numerals<p>
     * TST24.BF2 Traditional Chinese 24x24 F o n t (Big5)<p>
     * TSS24.BF2 Simplified Chinese 24x24 f o n t (GB code)<p></p>
     *
     * Rotation Rotate<p>
     * clockwise<p>
     * 0 non-rotation<p>
     * 90 Rotate 90 degrees clockwise<p>
     * 180 Rotate 180 degrees clockwise<p>
     * 270 Rotate 270 degrees clockwise <p></p>
     *
     * X-multiplication: Horizontal amplification,Maximum amplification 10 times the effective coefficient : 1~10.<p>
     * Y-multiplication: Vertical amplification 10 times the effective coefficient : 1~10.<p></p>
     *
     * Para imprimir un String que dentro de el tienes un saltoi de linea comunmente lo pondriamos como \n
     * pero ese saltod e linea la impresora lo interpreta como que ahi termina una instruccion entonces no imprime nada
     * para poner saltos de linea dentro del string hay que enviarselos a la impresora como Hexadecimal para ello debemos
     * escapar estos caracteres con \\uxxxx esto nos deja colocar el codigo hexadecimal del caracter del ASCII por ejemplo para
     * imprimir el simbolo "!" en la tabla ascii el hexadecimal es 20 para ello hay que poner
     * String auxiliarCredito += producto.getString("id_producto") + "\u0021"           <---- aqui es donde ponemos el simbolo en el caso yo queria saltos de linea
     * 							+ producto.getString("descripcion") + "\n"              <----- y ponia esto pero la impresora no hacia nada
     * 							+ producto.getString("talla")   + "\u000D"                    <------ entonces hay que poner esto! que equivale al salto de carro
     * 							+ producto.getString("color")                               <---- bueno resulta que ese comando 000D android studio lo marca como error, es muy probable que tenga algo mal en el encoding casual solo es ese xD
     * 							+ producto.getString("cantidad")        <---- entonces lo colocare en Octal \015
     * 							+ producto.getString("precio_etiqueta")
     * 							+ precioDistribucion
     * 							+ formaDePago;
     *
     * 					BUENO ni el octal funciono como retorno. el octal si lo envio android pero la impresora hace lo mismo apartir del salto de carro deja de impromir lo demas
     * 				aqui abajo los chinos disen que se use [R] pero no encuentor como enviarlo en android si lo mandas asi la impresora imprime [R]
     * Noted：
     * If the text includes double quotes ("), replace it with \["]<p>
     * To print 0D (hex) characters, use \[R] to print CR in the program<p>
     * To print 0A (hex) characters, use \[A] to print LF in the program<p>
     * The fifth letter of the English alphabet can only print capital letters<p></p>
     *
     *Example 1：<p>
     * SIZE 72 mm,60 mm<p>
     * CLS<p>
     * TEXT 10,10,"1",0,1,1,"AB0CDEFGHIJKLMNOPQRSTUVWXYZ"<p>
     * TEXT 10,30,"2",0,1,1,"AB0CDEFGHIJKLMNOPQRSTUVWXYZ"<p>
     * TEXT 10,60,"3",0,1,1,"AB012CDEF"<p>
     * TEXT 10,90,"4",0,1,1,"AB012CDEF456UVWXYZ"<p>
     * TEXT 10,130,"5",0,1,1,"AB0CDEFGHIJKLMNOPQRSTUVWXYZ"<p>
     * TEXT 10,190,"6",0,1,1,"AB0CDEFGHIJKLMNOPQRSTUVWXYZ"<p>
     * TEXT 10,220,"7",0,1,1,"AB0CDEFGHIJKLMNOPQRSTUVWXYZ"<p>
     * TEXT 10,250,"8",0,1,1,"AB0CDEFGHIJKLMNOPQRSTUVWXYZ"<p>
     * TEXT 10,280,"0",0,1,1,"AB0CDEFGHIJKLMNOPQRSTUVWXYZ"<p>
     * TEXT 10,310,"9",0,1,1,"AB0CDEFGHIJKLMNOPQRSTUVWXYZ"<p>
     * TEXT 10,330,"TSS24.BF2",0,1,1,"欢迎使用电子标签打印机"<p>
     * PRINT 1,1<p>
     *
     *
     * @param xStartPositionDots Text box upper left corner X coordinate
     * @param yStartPositionDots Text box upper left corner Y coordinate
     * @param font Font name 1-2-3-4-5-6-7-8-9-TST24.BF2-TSS24.BF2
     * @param rotation 0 90 180 270
     * @param xMultiplication Horizontal amplification, Maximum amplification 10 times the effective coefficient : 1~10.
     * @param yMultiplication Y-multiplication Vertical amplification 10 times the effective coefficient : 1~10.
     * @param content
     * @return
     */
    public static String printText(int xStartPositionDots, int yStartPositionDots,int font, int rotation,
                                   int xMultiplication, int yMultiplication, String content) {
        String comando = "TEXT %d,%d,\"%d\",%d,%d,%d,\"%s\"" + "\n";

        String temp = String.format(comando,xStartPositionDots,yStartPositionDots,font,rotation,xMultiplication,yMultiplication,content);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * No entendi su funcionamiento en el manual, no se si si se guarod en la impresora, o si no.
     * @param guardarEnFlash
     * @param fileName
     * @param programa
     * @return
     */
    public static String downloadProgram(boolean guardarEnFlash, String fileName, String programa){
        String comando="DOWNLOAD\n\"%s.BAS\" %s\nEOP"+ "\n";
        if(guardarEnFlash){
            comando = "DOWNLOAD\nF,\"%s.BAS\" %s\nEOP";
        }


        String temp = String.format(comando,fileName,programa);
        Log.d("comando", temp);
        return temp;
    }

    /**
     * No encontre como hacerlo funcionar no se si es por puerto serie
     * @param fileName
     * @return
     */
    public static String runAprogram(String fileName){
        String comando="RUN \"%s.BAS\""+ "\n";



        String temp = String.format(comando,fileName);
        Log.d("comando", temp);
        return temp;
    }







}