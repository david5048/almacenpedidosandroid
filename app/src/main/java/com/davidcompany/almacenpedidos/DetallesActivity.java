package com.davidcompany.almacenpedidos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.davidcompany.almacenpedidos.adapter.ProductosAdapter;
import com.davidcompany.almacenpedidos.adapter.TotalAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

public class DetallesActivity extends AppCompatActivity {

    public static JSONArray detallesPedido = new JSONArray();
    public static JSONObject totales = new JSONObject();
    public static JSONObject mensajesTotalesFinal = new JSONObject(); //contiene lo que debe mostrar el recuadrito que indica lo que el cliente debe pagar al recibir "mensajesFinalTotales":{"18":"Al recibir tu pedido deberás liquidar al agente Kaliope:","19":"Exceso de credito","20":"0% credito","21":"Inversion","22":"por liquidar al recibir el pedido","23":"En crédito Kaliope fecha de pago "}}



    String cuentaCliente = "0";
    String fechaPedido = "0";
    boolean bloqueo = false;  //si el pedido ya se puede comenzar a surtir esta en verdadero
    String mensajesAlmacenista = "";

    public static final String CLAVE_CUENTA = "NUMERO_CUENTA";
    public static final String CLAVE_FECHA_PEDIDO = "FECHA_PEDIDO";
    public static final String CLAVE_BLOQUEO = "BLOQUEO";
    public static final String CLAVE_MENSAJES_ALMACENISTA = "MENSAJES_ALMACENISTA";

    private final String URL_CONSULTAR_PEDIDO = "app_movil/app_almacen/consultarDetallePedido.php";
    private final String URL_SURTIR_PEDIDO = "app_movil/app_almacen/surtirPedido.php";

    ProgressDialog progressDialog;

    RecyclerView recyclerView;
    Button finalizar;

    Activity activity;


    ArrayList<HashMap<String,Object>> controlProductosConfirmados = new ArrayList<>();




    //========Bluetoot Printer======
    private static BluetoothSocket btsocket_bit;
    private static OutputStream btoutputstream_bit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles);
        activity = this;

        recyclerView = (RecyclerView) findViewById(R.id.detallesProductosRecyclerView);
        finalizar = (Button) findViewById(R.id.buttonFinalizar);
        FloatingActionButton fab = findViewById(R.id.fabImprimir );
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);


        Bundle bundle = getIntent().getExtras();

        if(bundle!=null){
            cuentaCliente = bundle.getString(CLAVE_CUENTA);
            fechaPedido = bundle.getString(CLAVE_FECHA_PEDIDO);
            bloqueo = bundle.getBoolean(CLAVE_BLOQUEO);
            mensajesAlmacenista = bundle.getString(CLAVE_MENSAJES_ALMACENISTA);
        }

        conectarServidor();



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            new AlertDialog.Builder(activity)
                    .setTitle("Confirmacion")
                    .setMessage("¿Quieres confirmar al pedido?\n\nAl hacerlo el pedido se movera a surtidos")
                    .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            conectarServidorSurtirpedido();
                        }
                    }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).create().show();

            }
        });

        finalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),PrintDemo.class);
                startActivity(intent);
            }
        });

    }


    private void conectarServidor(){
        RequestParams params = new RequestParams();
        params.put(CLAVE_CUENTA,cuentaCliente);
        params.put(CLAVE_FECHA_PEDIDO, fechaPedido);

        showProgresDialog(this);

        KaliopeServerClient.get(URL_CONSULTAR_PEDIDO,params,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progressDialog.dismiss();

                Log.d ("datosRecibidos",String.valueOf(response));
                //D/datosRecibidos: {"totales":{"nombre":"NANCY PORTILLO JULIaN","cuenta":"8608","limite_credito":"1500","grado":"VENDEDORA","dias":"14","ruta":"SAN LORENZO","porcentaje_apoyo_empresa":"1","porcentaje_pago_cliente":"0","numero_pedido":"1","fecha_entrega":"2021-06-16","fecha_pago_del_credito":"30-06-2021","suma_cantidad":4,"suma_credito":4,"suma_inversion":0,"cantidad_sin_confirmar":0,"suma_productos_etiqueta":766,"suma_productos_inversion":0,"suma_productos_credito":628,"suma_ganancia_cliente":138,"diferencia_credito":-872,"cantidad_pagar_cliente_credito":0,"cantidad_financiar_empresa":628,"pago_al_recibir":0,"mensaje_diferencia_credito":"Aun dispones de $872 en tu credito Kaliope"},
                // "detalles":[{"id":"292","no_pedido":"1","fecha_entrega_pedido":"2021-06-16","no_cuenta":"8608","nombre_cliente":"NANCY PORTILLO JULIaN","credito_cliente":"1500","grado_cliente":"VENDEDORA","ruta":"SAN LORENZO","id_producto":"BR1002","descripcion":"BRASSIER DAMA","talla":"36-B","cantidad":"1","color":"NEGRO","no_color":"rgb(25, 25, 25)","precio_etiqueta":"149","precio_vendedora":"115","precio_socia":"110","precio_empresaria":"106","precio_inversionista":"99","imagen_permanente":"fotos\/BR1002-NEGRO-1.jpg","producto_confirmado":"true","estado_producto":"CREDITO","seguimiento_producto":"Listo este producto ya se encuentra apartado lo recibiras en los proximos dias","almacen":null,"ganancia":34,"ganancia_inversion":50},{"id":"293","no_pedido":"1","fecha_entrega_pedido":"2021-06-16","no_cuenta":"8608","nombre_cliente":"NANCY PORTILLO JULIaN","credito_cliente":"1500","grado_cliente":"VENDEDORA","ruta":"SAN LORENZO","id_producto":"PT1002","descripcion":"Pantaleta Dama","talla":"G","cantidad":"1","color":"NEGRO","no_color":"rgb(36, 48, 64)","precio_etiqueta":"79","precio_vendedora":"62","precio_socia":"59","precio_empresaria":"58","precio_inversionista":"50","imagen_permanente":"fotos\/PT1002-NEGRO-1.jpg","producto_confirmado":"true","estado_producto":"CREDITO","seguimiento_producto":"Listo este producto ya se encuentra apartado lo recibiras en los proximos dias","almacen":null,"ganancia":17,"ganancia_inversion":29},{"id":"299","no_pedido":"1","fecha_entrega_pedido":"2021-06-16","no_cuenta":"8608","nombre_cliente":"NANCY PORTILLO JULIaN","credito_cliente":"1500","grado_cliente":"VENDEDORA","ruta":"SAN LORENZO","id_producto":"PT1002","descripcion":"Pantaleta Dama","talla":"G","cantidad":"1","color":"CAFE","no_color":"rgb(162, 127, 99)","precio_etiqueta":"79","precio_vendedora":"62","precio_socia":"59","precio_empresaria":"58","precio_inversionista":"50","imagen_permanente":"fotos\/PT1002-CAFE-1.jpg","producto_confirmado":"true","estado_producto":"CREDITO","seguimiento_producto":"Listo este producto ya se encuentra apartado lo recibiras en los proximos dias","almacen":null,"ganancia":17,"ganancia_inversion":29},{"id":"325","no_pedido":"1","fecha_entrega_pedido":"2021-06-16","no_cuenta":"8608","nombre_cliente":"NANCY PORTILLO JULIaN","credito_cliente":"1500","grado_cliente":"VENDEDORA","ruta":"SAN LORENZO","id_producto":"PD1001","descripcion":"PANTALON DAMA","talla":"34","cantidad":"1","color":"AZUL","no_color":"rgb(24, 37, 80)","precio_etiqueta":"459","precio_vendedora":"389","precio_socia":"383","precio_empresaria":"376","precio_inversionista":"357","imagen_permanente":"fotos\/PD1001-AZUL-1.jpg","producto_confirmado":"true","estado_producto":"CREDITO","seguimiento_producto":"Listo este producto ya se encuentra apartado lo recibiras en los proximos dias","almacen":null,"ganancia":70,"ganancia_inversion":102}],
                // "mensajesFinalTotales":{"18":"Al recibir tu pedido deberás liquidar al agente Kaliope:","19":"Exceso de credito","20":"0% credito","21":"Inversion","22":"por liquidar al recibir el pedido","23":"Quedaran pendientes para pagarse el 30-06-2021"}}

                try {

                    totales = response.getJSONObject("totales");
                    detallesPedido = response.getJSONArray("detalles");
                    mensajesTotalesFinal = response.getJSONObject("mensajesFinalTotales");

                    //lLENAMOS NUESTRO MAPA QUE SIRVE PARA LLEVAR EL CONTROL DE SI EL PRODUCTO HA SIDO SURTIDO O NO
                    HashMap<String,Object> map;
                    for(int i =0; i<detallesPedido.length(); i++){
                        map = new HashMap<>();
                        String baseDatosId=detallesPedido.getJSONObject(i).getString("id");
                        int id = Integer.parseInt(baseDatosId);
                        map.put("ID_BASE_DATOS",baseDatosId);
                        map.put("CODIGO",detallesPedido.getJSONObject(i).getString("id_producto"));
                        map.put("CONFIRMADO",false);

                        controlProductosConfirmados.add(map);
                    }


                    Log.d("datosProces3",detallesPedido.toString());
                    Log.d("Map",controlProductosConfirmados.toString());

                    enaviarArecycler();
                } catch (JSONException e) {
                    e.printStackTrace();
                }





            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //cuando por se recibe como respuesta un objeto que no puede ser convertido a jsonData
                //es decir si se conecta al servidor, pero desde el retornamos un echo de error
                //con un simple String lo recibimos en este metodo, crei que lo recibiria en el metodo onSUcces que tiene como parametro el responseString
                //pero parese que no, lo envia a este onFaiulure con Status Code

                //Si el nombre del archivo php esta mal para el ejemplo el correcto es: comprobar_usuario_app_kaliope.php
                // y el incorrecto es :comprobar_usuario_app_kaliop.php se llama a este metodo y entrega el codigo 404
                //lo que imprime en el log es un codigo http donde dice que <h1>Object not found!</h1>
                //            <p>
                //
                //
                //                The requested URL was not found on this server.
                //
                //
                //
                //                If you entered the URL manually please check your
                //                spelling and try again.
                //es decir si se encontro conexion al servidor y este respondio con ese mensaje
                //tambien si hay errores con alguna variable o algo asi, en este medio retorna el error como si lo viernas en el navegador
                //te dice la linea del error etc.


                String info = "Status Code: " + String.valueOf(statusCode) +"  responseString: " + responseString;
                Log.d("onFauile 1" , info);
                //Toast.makeText(MainActivity.this,responseString + "  Status Code: " + String.valueOf(statusCode) , Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
                utilidadesApp.dialogoResultadoConexion(activity,"Fallo de conexion", responseString + "\nStatus Code: " + String.valueOf(statusCode));


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                //cuando no se ha podido conectar con el servidor el statusCode=0 cz.msebera.android.httpclient.conn.ConnectTimeoutException: Connect to /192.168.1.10:8080 timed out
                //para simular esto estoy en un servidor local, obiamente el celular debe estar a la misma red, lo desconecte y lo movi a la red movil

                //cuando no hay coneccion a internet apagados datos y wifi se llama al metodo retry 5 veces y arroja la excepcion:
                // java.net.ConnectException: failed to connect to /192.168.1.10 (port 8080) from /:: (port 0) after 10000ms: connect failed: ENETUNREACH (Network is unreachable)


                //Si la url principal del servidor esta mal para simularlo cambiamos estamos a un servidor local con:
                //"http://192.168.1.10:8080/KALIOPE/" cambiamos la ip a "http://192.168.1.1:8080/KALIOPE/";
                //se llama al onRetry 5 veces y se arroja la excepcion en el log:
                //estatus code: 0 java.net.ConnectException: failed to connect to /192.168.1.1 (port 8080) from /192.168.1.71 (port 36134) after 10000ms: isConnected failed: EHOSTUNREACH (No route to host)
                //no hay ruta al Host

                //Si desconectamos el servidor de la ip antes la ip en el servidor de la computadora era 192.168.1.10, lo movimos a 192.168.1.1
                //genera lo mismo como si cambiaramos la ip en el programa android la opcion dew arriba. No
                //StatusCode0  Twhowable:   java.net.ConnectException: failed to connect to /192.168.1.10 (port 8080) from /192.168.1.71 (port 37786) after 10000ms: isConnected failed: EHOSTUNREACH (No route to host)
                //Llamo a reatry 5 veces


                String info = "StatusCode" + String.valueOf(statusCode) +"  Twhowable:   "+  throwable.toString();
                Log.d("onFauile 2" , info);
                //Toast.makeText(MainActivity.this,info, Toast.LENGTH_LONG).show();
                progressDialog.dismiss();

                utilidadesApp.dialogoResultadoConexion(activity,"Fallo de conexion", errorResponse + "\nStatus Code: " + String.valueOf(statusCode));

            }


            @Override
            public void onRetry(int retryNo) {
                progressDialog.setMessage("Reintentando conexion No: " + retryNo);
            }
        });

    }

    private void conectarServidorSurtirpedido(){
        RequestParams params = new RequestParams();
        params.put(CLAVE_CUENTA,cuentaCliente);
        params.put(CLAVE_FECHA_PEDIDO, fechaPedido);

        showProgresDialog(this);

        KaliopeServerClient.get(URL_SURTIR_PEDIDO,params,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progressDialog.dismiss();

                Log.d ("datosRecibidos",String.valueOf(response));

                try {

                    String estatus = response.getString("estatus");
                    String mensaje = response.getString("mensaje");



                    if(estatus.equalsIgnoreCase("EXITO")){
                        startActivity(new Intent(getApplicationContext(),PrintDemo.class));
                        Log.d("datosProces3",detallesPedido.toString());
                    }else{
                        utilidadesApp.dialogoResultadoConexion(activity,estatus,mensaje);
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }





            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //cuando por se recibe como respuesta un objeto que no puede ser convertido a jsonData
                //es decir si se conecta al servidor, pero desde el retornamos un echo de error
                //con un simple String lo recibimos en este metodo, crei que lo recibiria en el metodo onSUcces que tiene como parametro el responseString
                //pero parese que no, lo envia a este onFaiulure con Status Code

                //Si el nombre del archivo php esta mal para el ejemplo el correcto es: comprobar_usuario_app_kaliope.php
                // y el incorrecto es :comprobar_usuario_app_kaliop.php se llama a este metodo y entrega el codigo 404
                //lo que imprime en el log es un codigo http donde dice que <h1>Object not found!</h1>
                //            <p>
                //
                //
                //                The requested URL was not found on this server.
                //
                //
                //
                //                If you entered the URL manually please check your
                //                spelling and try again.
                //es decir si se encontro conexion al servidor y este respondio con ese mensaje
                //tambien si hay errores con alguna variable o algo asi, en este medio retorna el error como si lo viernas en el navegador
                //te dice la linea del error etc.


                String info = "Status Code: " + String.valueOf(statusCode) +"  responseString: " + responseString;
                Log.d("onFauile 1" , info);
                //Toast.makeText(MainActivity.this,responseString + "  Status Code: " + String.valueOf(statusCode) , Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
                utilidadesApp.dialogoResultadoConexion(activity,"Fallo de conexion", responseString + "\nStatus Code: " + String.valueOf(statusCode));


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                //cuando no se ha podido conectar con el servidor el statusCode=0 cz.msebera.android.httpclient.conn.ConnectTimeoutException: Connect to /192.168.1.10:8080 timed out
                //para simular esto estoy en un servidor local, obiamente el celular debe estar a la misma red, lo desconecte y lo movi a la red movil

                //cuando no hay coneccion a internet apagados datos y wifi se llama al metodo retry 5 veces y arroja la excepcion:
                // java.net.ConnectException: failed to connect to /192.168.1.10 (port 8080) from /:: (port 0) after 10000ms: connect failed: ENETUNREACH (Network is unreachable)


                //Si la url principal del servidor esta mal para simularlo cambiamos estamos a un servidor local con:
                //"http://192.168.1.10:8080/KALIOPE/" cambiamos la ip a "http://192.168.1.1:8080/KALIOPE/";
                //se llama al onRetry 5 veces y se arroja la excepcion en el log:
                //estatus code: 0 java.net.ConnectException: failed to connect to /192.168.1.1 (port 8080) from /192.168.1.71 (port 36134) after 10000ms: isConnected failed: EHOSTUNREACH (No route to host)
                //no hay ruta al Host

                //Si desconectamos el servidor de la ip antes la ip en el servidor de la computadora era 192.168.1.10, lo movimos a 192.168.1.1
                //genera lo mismo como si cambiaramos la ip en el programa android la opcion dew arriba. No
                //StatusCode0  Twhowable:   java.net.ConnectException: failed to connect to /192.168.1.10 (port 8080) from /192.168.1.71 (port 37786) after 10000ms: isConnected failed: EHOSTUNREACH (No route to host)
                //Llamo a reatry 5 veces


                String info = "StatusCode" + String.valueOf(statusCode) +"  Twhowable:   "+  throwable.toString();
                Log.d("onFauile 2" , info);
                //Toast.makeText(MainActivity.this,info, Toast.LENGTH_LONG).show();
                progressDialog.dismiss();

                utilidadesApp.dialogoResultadoConexion(activity,"Fallo de conexion", errorResponse + "\nStatus Code: " + String.valueOf(statusCode));

            }


            @Override
            public void onRetry(int retryNo) {
                progressDialog.setMessage("Reintentando conexion No: " + retryNo);
            }
        });

    }

    private void enaviarArecycler() {
        TotalAdapter totalAdapter = new TotalAdapter(totales,mensajesTotalesFinal);
        ProductosAdapter productosAdapter = new ProductosAdapter(detallesPedido,controlProductosConfirmados);
        totalAdapter.setExtrasNuevaApp(bloqueo,mensajesAlmacenista);                            //para mostrar el mensajito en totales adapter
        ConcatAdapter concatAdapter = new ConcatAdapter(totalAdapter,productosAdapter);
        recyclerView.setAdapter(concatAdapter);
    }


    private void showProgresDialog(Activity activity){

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Conectando al Servidor");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

    }








    /** Metodos para imprimir*/


    protected void connect() {


        if(btsocket_bit == null){
            Intent BTIntent = new Intent(getApplicationContext(), DeviceList.class);
            this.startActivityForResult(BTIntent, DeviceList.REQUEST_CONNECT_BT);
        }
        else{
            OutputStream opstream = null;
            try {
                opstream = btsocket_bit.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            btoutputstream_bit = opstream;

            try {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                btoutputstream_bit = btsocket_bit.getOutputStream();


                resetPrint();
                btoutputstream_bit.write(PrinterCommands.ESC_ALIGN_CENTER);
                printText("Kaliope Distribuidora SA de CV\n");
                printNewLine();

                btoutputstream_bit.write(PrinterCommands.ESC_ALIGN_LEFT);
                printPhoto();

                printNewLine();
                //btoutputstream_bit.write(PrinterCommands.ESC_ALIGN_LEFT);
                btoutputstream_bit.write(PrinterCommands.ESC_ALIGN_CENTER);
                printText("Numero de tarjeta:" + "\n");
                btoutputstream_bit.write(PrinterCommands.SELECT_FONT_A);
                printNewLine();
                printTitle( "4772-1430-0714-4799" + "\n");
                btoutputstream_bit.write(PrinterCommands.SELECT_FONT_A);


                printNewLine();
                printNewLine();
                //btoutputstream_bit.write(PrinterCommands.ESC_ALIGN_LEFT);
                btoutputstream_bit.write(PrinterCommands.ESC_ALIGN_CENTER);
                printText("Numero cuenta BBVA (Bancomer):" + "\n");
                btoutputstream_bit.write(PrinterCommands.SELECT_FONT_A);
                printNewLine();
                printTitle( "0103377695" + "\n");
                btoutputstream_bit.write(PrinterCommands.SELECT_FONT_A);

                printNewLine();
                printNewLine();
                //btoutputstream_bit.write(PrinterCommands.ESC_ALIGN_LEFT);
                btoutputstream_bit.write(PrinterCommands.ESC_ALIGN_CENTER);
                printText("Cuenta CLABE:" + "\n");
                btoutputstream_bit.write(PrinterCommands.SELECT_FONT_A);
                printNewLine();
                printTitle( "012426001033776956" + "\n");
                btoutputstream_bit.write(PrinterCommands.SELECT_FONT_A);


                printNewLine();
                printNewLine();
                printNewLine();
                //btoutputstream_bit.write(PrinterCommands.ESC_SETTING_BOLD);
                printText( "Estimado cliente con este numero\n" +
                        "de tarjeta y cuenta, podra hacer\n" +
                        "depocitos en cualquier OXXO\n" +
                        "o directo en ventanilla\n" +
                        "BBVA (BANCOMER).\n\n" +
                        "Con la CLABE podra hacer\n" +
                        "transferencias interbancarias\n\n"+
                        "Una vez hecho el depocito\n" +
                        "guarde el comprobate para\n" +
                        "entregarcelo a su agente\n" +
                        "de ventas, o comuniquese\n" +
                        "directo a Kaliope para\n" +
                        "informar sobre el depocito\n\n" +
                        "Estamos para servirle en el\n" +
                        "telefono de oficina:\n" +
                        "           712-159-07-29");
                btoutputstream_bit.write(PrinterCommands.SELECT_FONT_A);



                resetPrint();

                //printUnicode();


                printNewLine();
                printNewLine();
                printNewLine();
                printNewLine();
                printNewLine();
                printNewLine();
                printNewLine();
                printNewLine();


                btoutputstream_bit.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //print Title
    private void printTitle(String msg) {
        try {
            //Print config
            byte[] bb = new byte[]{0x1B,0x21,0x08};
            byte[] bb2 = new byte[]{0x1B,0x21,0x20};
            byte[] bb3 = new byte[]{0x1B,0x21,0x10};
            byte[] cc = new byte[]{0x1B,0x21,0x00};

            btoutputstream_bit.write(bb3);

            //set text into center
            btoutputstream_bit.write(PrinterCommands.ESC_ALIGN_CENTER);
            btoutputstream_bit.write(msg.getBytes());
            printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //print photo
    public void printPhoto() {
        try {
            Bitmap bmp = BitmapFactory.decodeResource(getResources(),R.drawable.logokaliopeticketjustificacionizq);

            if(bmp!=null){
                byte[] command = Utils.decodeBitmap(bmp);
                printText(command);
            }else{
                Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PrintTools", "the file isn't exists");
        }
    }

    //print unicode
    public void printUnicode(){
        try {
            btoutputstream_bit.write(PrinterCommands.ESC_ALIGN_CENTER);
            printText(Utils.UNICODE_TEXT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //print new line
    private void printNewLine() {
        try {
            btoutputstream_bit.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void resetPrint() {
        try{
            btoutputstream_bit.write(PrinterCommands.ESC_FONT_COLOR_DEFAULT);
            btoutputstream_bit.write(PrinterCommands.FS_FONT_ALIGN);
            btoutputstream_bit.write(PrinterCommands.ESC_ALIGN_LEFT);
            btoutputstream_bit.write(PrinterCommands.ESC_CANCEL_BOLD);
            btoutputstream_bit.write(PrinterCommands.SELECT_FONT_A);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //print text
    private void printText(String msg) {
        try {
            // Print normal text
            btoutputstream_bit.write(msg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //print byte[]
    private void printText(byte[] msg) {
        try {
            // Print normal text
            btoutputstream_bit.write(msg);
            printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            btsocket_bit = DeviceList.getSocket();
            if(btsocket_bit != null){
                /*printText(message.getText().toString());*/
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if(btsocket_bit!= null){
                btoutputstream_bit.close();
                btsocket_bit.close();
                btsocket_bit = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}