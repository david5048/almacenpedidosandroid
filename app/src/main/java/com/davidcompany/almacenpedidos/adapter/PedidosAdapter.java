package com.davidcompany.almacenpedidos.adapter;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.davidcompany.almacenpedidos.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

/*
Para crear un On >CLick listener al aprecer de la mejor practica y manera mas eficiente
https://www.youtube.com/watch?v=69C1ljfDvl0
 */

public class PedidosAdapter extends RecyclerView.Adapter<PedidosAdapter.ViewHolderPedido> {
    JSONArray pedidos;
    Animation latido;

    //====9=====
    private OnPedidoListener myOnPedidoListener;

    //====10==== Modificamso el constructor para solicitar como parametro el listener
    public PedidosAdapter(JSONArray pedidos, OnPedidoListener onPedidoListener) {
        this.pedidos = pedidos;
        this.myOnPedidoListener=onPedidoListener;
    }

    @NonNull
    @Override
    public ViewHolderPedido onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_container_pedido,
                parent,
                false
        );

        //====11====
        return new ViewHolderPedido(view, myOnPedidoListener);

    }

    @Override
    public void onBindViewHolder(@NonNull PedidosAdapter.ViewHolderPedido holder, int position) {

        try {
            JSONObject pedido = pedidos.getJSONObject(position);
            // {"fecha_entrega_pedido":"2021-06-27","no_cuenta":"5838","nombre_cliente":"ANGELICA MORALES VILLEGAS ","credito_cliente":"2700"
            // ,"grado_cliente":"SOCIA","ruta":"SAN LUIS DE LA PAZ","plataforma":"","sumaCantidad":"8","fechaCierre":"24-07-2021",
            // "diasAntes":"2","diasRestantesParaPoderSurtir":3,"bloqueo":true,"mensajesAlAlmacenista":"Ya puedes surtir este pedido!!"}


            String fechaEntrega = pedido.getString("fecha_entrega_pedido"); //31-05-2021
            boolean bloqueado = pedido.getBoolean("bloqueo");
            String mensajesAlmacenista = pedido.getString("mensajesAlAlmacenista");


            Date auxiliar = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd"); //el formato de fecha que va a recibir
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MM-yyyy"); //el formato de fecha que va a salir
            auxiliar = simpleDateFormat.parse(fechaEntrega);
            String fechaFormateada = simpleDateFormat1.format(auxiliar);


            holder.cuentaCliente.setText(pedido.getString("no_cuenta"));
            holder.nombreCliente.setText(pedido.getString("nombre_cliente"));
            holder.mensajeAlmacenista.setText(mensajesAlmacenista);
            holder.fechaEntrega.setText(fechaFormateada);
            holder.fechaCierre.setText(pedido.getString("fechaCierre"));
            holder.limiteCredito.setText(pedido.getString("credito_cliente"));
            holder.gradoCliente.setText(pedido.getString("grado_cliente"));
            holder.cantidad.setText(pedido.getString("sumaCantidad"));
            holder.zona.setText(pedido.getString("ruta"));
            holder.sucursal.setText(pedido.getString("sucursal"));
            holder.diasBloqueo.setText(pedido.getString("diasAntes"));
            holder.plataforma.setText(pedido.getString("plataforma"));



            if(bloqueado){
                holder.constraintLayout.setBackgroundColor(holder.itemView.getResources().getColor(R.color.confirmado));
                holder.mensajeAlmacenista.setTextColor(Color.BLACK);
                holder.mensajeAlmacenista.setTextSize(20);
                holder.mensajeAlmacenista.startAnimation(latido);
                holder.mensajeAlmacenista.setBackgroundResource(R.drawable.background_redondo);
            }else{
                holder.constraintLayout.setBackgroundColor(Color.TRANSPARENT);
                holder.mensajeAlmacenista.setTextColor(Color.RED);
                holder.mensajeAlmacenista.setTextSize(16);
                holder.mensajeAlmacenista.clearAnimation();
                holder.mensajeAlmacenista.setBackgroundColor(Color.TRANSPARENT);

                if(mensajesAlmacenista.equals("Pedido Surtido")){
                    holder.mensajeAlmacenista.setTextSize(35);
                    holder.mensajeAlmacenista.setTextColor(Color.BLACK);
                }
            }

            if(pedido.getString("plataforma").equals("appAndroid")){
                holder.plataforma.setTextColor(Color.parseColor("#FF0022FF"));
            }else{
                holder.plataforma.setTextColor(Color.parseColor("#FFDD35FA"));
            }




        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return pedidos.length();
    }

    public class ViewHolderPedido extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView cuentaCliente,
                nombreCliente,
                limiteCredito,
                gradoCliente,
                zona,
                sucursal,
                cantidad,
                fechaEntrega,
                fechaCierre,
                mensajeAlmacenista,
                diasBloqueo,
                plataforma;

        ConstraintLayout constraintLayout;


        //====5=====
        OnPedidoListener onPedidoListener;


        //====6==== solicitar ese onPedidoListener como parametro del constructor del view Holder
public ViewHolderPedido(@NonNull View itemView, OnPedidoListener onPedidoListener) {
            super(itemView);
            cuentaCliente = (TextView) itemView.findViewById(R.id.icp_cuentaClienteTV);
            nombreCliente = (TextView) itemView.findViewById(R.id.icp_nombreClienteTV);
            limiteCredito = (TextView) itemView.findViewById(R.id.icp_limiteCreditoTV);
            gradoCliente = (TextView) itemView.findViewById(R.id.icp_gradoTV);
            zona = (TextView) itemView.findViewById(R.id.icp_nombreZonaTV);
            sucursal = (TextView) itemView.findViewById(R.id.icp_sucursal);
            cantidad = (TextView) itemView.findViewById(R.id.icp_piezas);
            fechaEntrega = (TextView) itemView.findViewById(R.id.icp_fechaEntregaTV);
            fechaCierre = (TextView) itemView.findViewById(R.id.icp_fechaCierreTV);
            mensajeAlmacenista = (TextView) itemView.findViewById(R.id.icp_mensajeAlmacenista);
            constraintLayout = (ConstraintLayout) itemView.findViewById(R.id.icp_constrainLayout);
            diasBloqueo = (TextView) itemView.findViewById(R.id.icp_diasBloqueoTV);
            plataforma = (TextView) itemView.findViewById(R.id.icp_plataformaTV);

            latido = AnimationUtils.loadAnimation(itemView.getContext(),R.anim.latido);
            latido.setFillAfter(true);//para que se quede donde termina la anim
            latido.setRepeatMode(Animation.REVERSE); //modo de repeticion, en el reverse se ejecuta la animacion y cuando termine de ejecutarse va  adar reversa
            latido.setRepeatCount(Animation.INFINITE); //cuantas veces queremos que se repita la animacion, podria ser un numero entero 20 para 20 veces por ejemplo




            //=====7====
            this.onPedidoListener = onPedidoListener;

            //====4====
            //aqui relacionamos o enlasamos el escuchador a el item que toca, porque es una iteracion aqui recordar
            itemView.setOnClickListener(this);
        }




        //====3=====
        //este metodo fue el que implementamos y sobreescribiremos para que escuche nuestros clics
        //ahora enlasaremos nuestro recycler view completo al click listener para que este escuchando en todos los item el evento
        @Override
        public void onClick(View v) {
        //====8====
            //Ahora queremos que en este onclick se llame al nuestro y le pasamos la psoicion del adaptador
            onPedidoListener.onPedidoClick(getAbsoluteAdapterPosition());

        }
    }




    //====1====
    //para crear el onclick listener
    //ya declaramos esta interfas con el metodo abstracto ahora debemos hacer que este escuchando los clic en neustro recycler
    //y nos vamos a la clase ViewHolderPedido e implementamos la clase On Click Listener, y sobre escribimos los metodos
    // el paso 2 se hace en el mainActivytyRecycler
    public interface OnPedidoListener{
        void onPedidoClick(int position);
    }
}
