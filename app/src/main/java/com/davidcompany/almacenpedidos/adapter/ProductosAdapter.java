package com.davidcompany.almacenpedidos.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.davidcompany.almacenpedidos.KaliopeServerClient;
import com.davidcompany.almacenpedidos.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ProductosAdapter extends RecyclerView.Adapter<ProductosAdapter.ViewHolderProducto> {
    JSONArray listaCarrito;
    /*
            {"carritoCliente":[ {"id":"1675","no_pedido":null,"fecha_entrega_pedido":"2021-07-27","no_cuenta":"7690","nombre_cliente":"MIRIAN PEREZ GARCIA","credito_cliente":"1500","grado_cliente":"SOCIA","ruta":"SANTIAGO UNDAMEO","id_producto":"SM0359-N","descripcion":"SUETER DAMA","talla":"UNT","cantidad":"1","color":"NEGRO","no_color":"rgb(25, 25, 25)","precio_etiqueta":"359","precio_vendedora":"279","precio_socia":"271","precio_empresaria":"263","precio_inversionista":"245","imagen_permanente":"fotos\/SM0359-NEGRO-1.jpg","producto_confirmado":"true","estado_producto":"CREDITO","seguimiento_producto":"El producto ha sido apartado en almacen","almacen":null,"plataforma":"appAndroid","fecha_captura":"2021-07-20","ganancia":88,"ganancia_inversion":114},
            {"id":"1676","no_pedido":null,"fecha_entrega_pedido":"2021-07-27","no_cuenta":"7690","nombre_cliente":"MIRIAN PEREZ GARCIA","credito_cliente":"1500","grado_cliente":"SOCIA","ruta":"SANTIAGO UNDAMEO","id_producto":"SM0359-N","descripcion":"SUETER DAMA","talla":"UNT","cantidad":"1","color":"NEGRO","no_color":"rgb(25, 25, 25)","precio_etiqueta":"359","precio_vendedora":"279","precio_socia":"271","precio_empresaria":"263","precio_inversionista":"245","imagen_permanente":"fotos\/SM0359-NEGRO-1.jpg","producto_confirmado":"true","estado_producto":"CREDITO","seguimiento_producto":"El producto ha sido apartado en almacen","almacen":null,"plataforma":"appAndroid","fecha_captura":"2021-07-20","ganancia":88,"ganancia_inversion":114},
            {"id":"1677","no_pedido":null,"fecha_entrega_pedido":"2021-07-27","no_cuenta":"7690","nombre_cliente":"MIRIAN PEREZ GARCIA","credito_cliente":"1500","grado_cliente":"SOCIA","ruta":"SANTIAGO UNDAMEO","id_producto":"SM0359-N","descripcion":"SUETER DAMA","talla":"UNT","cantidad":"1","color":"NEGRO","no_color":"rgb(25, 25, 25)","precio_etiqueta":"359","precio_vendedora":"279","precio_socia":"271","precio_empresaria":"263","precio_inversionista":"245","imagen_permanente":"fotos\/SM0359-NEGRO-1.jpg","producto_confirmado":"true","estado_producto":"CREDITO","seguimiento_producto":"El producto ha sido apartado en almacen","almacen":null,"plataforma":"appAndroid","fecha_captura":"2021-07-20","ganancia":88,"ganancia_inversion":114},

             */

    ArrayList <HashMap<String,Object>> controlProductosConfirmados;
    HashMap<String,Object> map;

    Activity activity;
    ProgressDialog progressDialog;

    TotalAdapter totalAdapter;          //solicitamos una referencia de nuestro totalAdapter para poder solicitar una actualizacion de ese adaptador

    public ProductosAdapter(JSONArray listaCarrito, ArrayList<HashMap<String,Object>> list) {
        this.listaCarrito = listaCarrito;
        this.controlProductosConfirmados=list;
    }

    @NonNull
    @Override
    public ViewHolderProducto onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_container_producto,
                parent,
                false
        );
        return new ViewHolderProducto(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderProducto holder, int position) {
        try {
            Log.d("carritoAdapter", listaCarrito.getJSONObject(position).toString());
            //es donde actualizamos la vista de cada item
            JSONObject jsonObject = listaCarrito.getJSONObject(position);
            Log.d("temporal", jsonObject.toString());

            String urlImagenPermanente = KaliopeServerClient.BASE_URL + jsonObject.getString("imagen_permanente");

            Glide.with(holder.itemView)
                    .load(urlImagenPermanente)
                    .thumbnail(.1f)                                     //Mostramos la imagen al 10% de su resolucion en el carrito hasta que la imagen este lista
                    .into(holder.imagenPermanente);

            holder.tvId.setText(jsonObject.getString("id"));
            holder.tvIdProducto.setText(jsonObject.getString("id_producto"));
            holder.tvDescripcion.setText(jsonObject.getString("descripcion"));
            holder.tvCantidad.setText(jsonObject.getString("cantidad"));
            holder.tvTalla.setText(jsonObject.getString("talla"));
            holder.tvColor.setText(jsonObject.getString("color"));
            holder.tvPrecioPublico.setText(jsonObject.getString("precio_etiqueta"));




            String gradoCliente = jsonObject.getString("grado_cliente");
            String formaDePago = jsonObject.getString("estado_producto");                     //aqui recibimos CREDITO INVERSION AGOTADO jaja yo se el agotado rompe las normas pero hay que hacerlo
            String limiteDeCretido = jsonObject.getString("credito_cliente");
            String idDataBase = jsonObject.getString("id");
            String productoConfirmado = jsonObject.getString("producto_confirmado");          //obtenemos en texto true or false dependiendo de si el producto fue confirmado



            String ganancia = jsonObject.getString("ganancia");
            String gananciaInversion = jsonObject.getString("ganancia_inversion");


            String plataforma = jsonObject.getString("plataforma");

            //obtenemos el precio de distribucion segun el grado o forma de pago del cliente y a su ves activamos los botones
            if(formaDePago.equals("CREDITO")){


                holder.tvFormaPago.setText(formaDePago);
                holder.tvFormaPago.setTextColor(Color.GREEN);
                holder.tvGanancia.setText(ganancia);


                if(gradoCliente.equals("VENDEDORA")){
                    holder.tvPrecioDistribucion.setText(jsonObject.getString("precio_vendedora"));

                }else if(gradoCliente.equals("SOCIA")){
                    holder.tvPrecioDistribucion.setText(jsonObject.getString("precio_socia"));

                }else if(gradoCliente.equals("EMPRESARIA")){
                    holder.tvPrecioDistribucion.setText(jsonObject.getString("precio_empresaria"));
                }


            }else if(formaDePago.equals("AGOTADO")){

                holder.tvFormaPago.setText(formaDePago);
                holder.tvFormaPago.setTextColor(Color.RED);
                holder.tvGanancia.setText("");

            }


            holder.tvPlataforma.setText(plataforma);
            if(plataforma.equals("appAndroid")){
                holder.tvPlataforma.setTextColor(Color.parseColor("#FF0022FF"));
            }else{
                holder.tvPlataforma.setTextColor(Color.parseColor("#FFDD35FA"));
            }







            map = controlProductosConfirmados.get(position);
            try {
                boolean confirm =(boolean) map.get("CONFIRMADO");
                if(confirm){
                    holder.checkBoxSurtido.setChecked(confirm);
                    holder.cardView.setCardBackgroundColor(holder.cardView.getResources().getColor(R.color.confirmado));
                }else{
                    holder.cardView.setCardBackgroundColor(Color.WHITE);
                    holder.checkBoxSurtido.setChecked(confirm);
                }

            } catch (NullPointerException e) {
                e.printStackTrace();
            }


            Log.d("Posicion",String.valueOf(position));



            holder.checkBoxSurtido.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean estatus = ((CheckBox) v).isChecked();

                    Log.d("Check", "Click en cheked");

                    Log.d("Check position",String.valueOf(position));
                    Log.d("Map",map.toString());
                    Log.d("ArrayListo",controlProductosConfirmados.toString());
                    controlProductosConfirmados.get(position).replace("CONFIRMADO",estatus);                                    //nos damos cuetna que como es por referencia, en este map el remplace afectara directo al map que esta en el array list, no es necesario a�adir a la lista el nuevo map
                    Log.d("ArrayListo",controlProductosConfirmados.toString());
                    notifyDataSetChanged();
                }
            });




        } catch (JSONException e) {
            e.printStackTrace();
        }






    }

    @Override
    public int getItemCount() {
        return listaCarrito.length();
    }

    public class ViewHolderProducto extends RecyclerView.ViewHolder {
        //contendremos todas las vistas en este view Holder
        TextView tvId,
                tvIdProducto,
                tvDescripcion,
                tvCantidad,
                tvTalla,
                tvColor,
                tvPrecioPublico,
                tvPrecioDistribucion,
                tvGanancia,
                tvFormaPago,
                tvPlataforma;



        ImageView imagenPermanente;

        CardView cardView;

        CheckBox checkBoxSurtido;
        
        
        public ViewHolderProducto(@NonNull View itemView) {
            super(itemView);

            tvId = (TextView) itemView.findViewById(R.id.icc_idBaseDatos);
            tvIdProducto = (TextView) itemView.findViewById(R.id.icc_idProducto);
            tvDescripcion = (TextView) itemView.findViewById(R.id.icc_descripcion);
            tvCantidad = (TextView) itemView.findViewById(R.id.icc_cantidad);
            tvTalla = (TextView) itemView.findViewById(R.id.icc_talla);
            tvColor = (TextView) itemView.findViewById(R.id.icc_color);
            tvPrecioPublico = (TextView) itemView.findViewById(R.id.icc_precioVenta);
            tvPrecioDistribucion = (TextView) itemView.findViewById(R.id.icc_precioDistribucion);
            tvGanancia = (TextView) itemView.findViewById(R.id.icc_ganancia);
            tvFormaPago = (TextView) itemView.findViewById(R.id.icc_formaPago);
            tvPlataforma = (TextView) itemView.findViewById(R.id.icc_plataformaTV);
            checkBoxSurtido= (CheckBox) itemView.findViewById(R.id.icc_checkSurtido);


            imagenPermanente = (ImageView) itemView.findViewById(R.id.icc_imagen);
            cardView = (CardView) itemView.findViewById(R.id.icc_cardView);






        }
    }
}
