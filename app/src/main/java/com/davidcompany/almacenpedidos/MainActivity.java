package com.davidcompany.almacenpedidos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener{

    private AppBarConfiguration mAppBarConfiguration;
    NavController navController;
    NavigationView navigationView;
    DrawerLayout drawer;
    Toolbar toolbar;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        activity = this;
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),PrintDemo.class));
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                 R.id.pedidosFragment)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);//el boton hamburgesita
        //NavigationUI.setupWithNavController(navigationView, navController);
        // esto lo añade cuando creas desde nueva activity un drawer ativity, esta linea hace que la navegacion por
        // los fragments lo haga en automatico el mobile_navigation.xml de acuerdo con los ids que escojes para
        //los items del menu, activity_main_drawer, el id del item del menu debe ser el mismo para el id del fragment
        //y en automatico cuando haces clicl envia a ese fragment, pero en este caso queiro enviarle acciones
        //al mismo fragment dependiendo de que menu del drawer se rpecione, por eso debi definir
        //onNavigationIntemSelected como lo hice en la app de los clientes
        //https://developer.android.com/guide/navigation/navigation-ui?hl=es-419
        //tambien mantiene actualizada la toolbar

        navigationView.setCheckedItem(R.id.pedidosFragment);
        getSupportActionBar().setSubtitle(PedidosFragment.FILTRO_CONFIRMADOS);//como iniciamos en confirmados ponemos ese subtitulo
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity2, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        //para enviar datos entre destinos, en este caso yo envio por bundle pero esta documentacion
        //podriamos enviarlos por grandle de una manera mas nice
        //https://developer.android.com/guide/navigation/navigation-pass-data?hl=es-419#java

        if(item.getItemId() == R.id.pedidosFragment){
            Bundle bundle = new Bundle();
            bundle.putString(PedidosFragment.FILTRO_KEY,PedidosFragment.FILTRO_CONFIRMADOS);
            navController.navigate(R.id.pedidosFragment,bundle);
        }



        if(item.getItemId() == R.id.pedidosFragmentSurtidos){
            Bundle bundle = new Bundle();
            bundle.putString(PedidosFragment.FILTRO_KEY,PedidosFragment.FILTRO_SURTIDOS);
            toolbar.setTitle(PedidosFragment.FILTRO_SURTIDOS);
            navController.navigate(R.id.pedidosFragment,bundle);
        }

        getSupportActionBar().setSubtitle(item.getTitle());



        drawer.closeDrawers();

        return true;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Toast.makeText(activity, "Para salir de la aplicacion presione el boton de home", Toast.LENGTH_SHORT).show();
    }
}