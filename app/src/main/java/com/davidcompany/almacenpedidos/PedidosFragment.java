package com.davidcompany.almacenpedidos;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.davidcompany.almacenpedidos.adapter.PedidosAdapter;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PedidosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PedidosFragment extends Fragment implements PedidosAdapter.OnPedidoListener{


    RecyclerView recyclerView;

    ProgressDialog progressDialog;

    JSONArray pedidos = new JSONArray();

    String filtro = "";
    public static final String FILTRO_CONFIRMADOS = "CONFIRMADOS";
    public static final String FILTRO_SIN_CONFIRMAR = "SIN_CONFIRMAR";
    public static final String FILTRO_SURTIDOS = "SURTIDOS";
    public static final String FILTRO_KEY = "FILTRO";

    private final String URL_CONSULTA_PEDIDOS = "app_movil/app_almacen/consultar_pedidos.php";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PedidosFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PedidosFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PedidosFragment newInstance(String param1, String param2) {
        PedidosFragment fragment = new PedidosFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().getString(FILTRO_KEY)!=null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            filtro = getArguments().getString(FILTRO_KEY);
            Log.d("FILTRO", filtro);
        }else{
            filtro = FILTRO_CONFIRMADOS;

            Log.d("FILTRO", filtro);
        }
        consultarPedidos();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pedidos, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //cuando ya tenemos la vista inflada este se llama inmediatamente, es mejor hacerlo aqui porque depronto
        //El onCreateView falla, en este casos abemos que ya se creo completamente la interfas

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler1);
        //recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        Log.d("fragment","OnViewCreated");

        //recuperamos el bundle
        if(getArguments().getString(FILTRO_KEY)!=null){
            //si hay argumentos
            filtro = getArguments().getString(FILTRO_KEY);
            Log.d("FILTRO", filtro);

        }






    }



    private void consultarPedidos(){
        RequestParams params = new RequestParams();
        params.put(FILTRO_KEY, filtro);

        showProgresDialog(getActivity());

        KaliopeServerClient.get(URL_CONSULTA_PEDIDOS,params,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {


                Log.d ("datosRecibidos",String.valueOf(response));
                //D/datosRecibidos:
                //{"pedidos":[{"fecha_entrega_pedido":"2021-07-22","no_cuenta":"1839","nombre_cliente":"MARIA CRISTINA ACEBEDO OLVERA","credito_cliente":"2200","grado_cliente":"EMPRESARIA","ruta":"HUICHAPAN","plataforma":"appAndroid","sumaCantidad":"7","fechaCierre":"21-07-2021","diasAntes":"1","diasRestantesParaPoderSurtir":6,"bloqueo":true,"mensajesAlAlmacenista":"Ya puedes surtir este pedido!!"},
                // {"fecha_entrega_pedido":"2021-07-21","no_cuenta":"5838","nombre_cliente":"ANGELICA MORALES VILLEGAS ","credito_cliente":"2700","grado_cliente":"SOCIA","ruta":"SAN LUIS DE LA PAZ","plataforma":"","sumaCantidad":"8","fechaCierre":"24-07-2021","diasAntes":"2","diasRestantesParaPoderSurtir":3,"bloqueo":true,"mensajesAlAlmacenista":"Ya puedes surtir este pedido!!"}]}

                try {

                    pedidos = response.getJSONArray("pedidos");
                    Log.d("datosProces3",pedidos.toString());


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                enaviarArecycler();



            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //cuando por se recibe como respuesta un objeto que no puede ser convertido a jsonData
                //es decir si se conecta al servidor, pero desde el retornamos un echo de error
                //con un simple String lo recibimos en este metodo, crei que lo recibiria en el metodo onSUcces que tiene como parametro el responseString
                //pero parese que no, lo envia a este onFaiulure con Status Code

                //Si el nombre del archivo php esta mal para el ejemplo el correcto es: comprobar_usuario_app_kaliope.php
                // y el incorrecto es :comprobar_usuario_app_kaliop.php se llama a este metodo y entrega el codigo 404
                //lo que imprime en el log es un codigo http donde dice que <h1>Object not found!</h1>
                //            <p>
                //
                //
                //                The requested URL was not found on this server.
                //
                //
                //
                //                If you entered the URL manually please check your
                //                spelling and try again.
                //es decir si se encontro conexion al servidor y este respondio con ese mensaje
                //tambien si hay errores con alguna variable o algo asi, en este medio retorna el error como si lo viernas en el navegador
                //te dice la linea del error etc.


                String info = "Status Code: " + String.valueOf(statusCode) +"  responseString: " + responseString;
                Log.d("onFauile 1" , info);
                //Toast.makeText(MainActivity.this,responseString + "  Status Code: " + String.valueOf(statusCode) , Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
                utilidadesApp.dialogoResultadoConexion(getActivity(),"Fallo de conexion", responseString + "\nStatus Code: " + String.valueOf(statusCode));


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                //cuando no se ha podido conectar con el servidor el statusCode=0 cz.msebera.android.httpclient.conn.ConnectTimeoutException: Connect to /192.168.1.10:8080 timed out
                //para simular esto estoy en un servidor local, obiamente el celular debe estar a la misma red, lo desconecte y lo movi a la red movil

                //cuando no hay coneccion a internet apagados datos y wifi se llama al metodo retry 5 veces y arroja la excepcion:
                // java.net.ConnectException: failed to connect to /192.168.1.10 (port 8080) from /:: (port 0) after 10000ms: connect failed: ENETUNREACH (Network is unreachable)


                //Si la url principal del servidor esta mal para simularlo cambiamos estamos a un servidor local con:
                //"http://192.168.1.10:8080/KALIOPE/" cambiamos la ip a "http://192.168.1.1:8080/KALIOPE/";
                //se llama al onRetry 5 veces y se arroja la excepcion en el log:
                //estatus code: 0 java.net.ConnectException: failed to connect to /192.168.1.1 (port 8080) from /192.168.1.71 (port 36134) after 10000ms: isConnected failed: EHOSTUNREACH (No route to host)
                //no hay ruta al Host

                //Si desconectamos el servidor de la ip antes la ip en el servidor de la computadora era 192.168.1.10, lo movimos a 192.168.1.1
                //genera lo mismo como si cambiaramos la ip en el programa android la opcion dew arriba. No
                //StatusCode0  Twhowable:   java.net.ConnectException: failed to connect to /192.168.1.10 (port 8080) from /192.168.1.71 (port 37786) after 10000ms: isConnected failed: EHOSTUNREACH (No route to host)
                //Llamo a reatry 5 veces


                String info = "StatusCode" + String.valueOf(statusCode) +"  Twhowable:   "+  throwable.toString();
                Log.d("onFauile 2" , info);
                //Toast.makeText(MainActivity.this,info, Toast.LENGTH_LONG).show();
                progressDialog.dismiss();

                utilidadesApp.dialogoResultadoConexion(getActivity(),"Fallo de conexion", errorResponse + "\nStatus Code: " + String.valueOf(statusCode));

            }


            @Override
            public void onRetry(int retryNo) {
                progressDialog.setMessage("Reintentando conexion No: " + retryNo);
            }
        });



    }


    private void showProgresDialog(Activity activity){

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Conectando al Servidor");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    private void enaviarArecycler(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        PedidosAdapter pedidosAdapter = new PedidosAdapter(pedidos,this);
        recyclerView.setAdapter(pedidosAdapter);
        progressDialog.dismiss();
    }

    @Override
    public void onPedidoClick(int position) {


        try {
            JSONObject pedido = pedidos.getJSONObject(position);
            String numeroCuenta = pedido.getString("no_cuenta");
            String fechaPedido = pedido.getString("fecha_entrega_pedido");
            boolean bloqueo = pedido.getBoolean("bloqueo");
            String mensajeAlmacenista = pedido.getString("mensajesAlAlmacenista");
            Bundle bundle = new Bundle();
            bundle.putString(DetallesActivity.CLAVE_CUENTA, numeroCuenta);
            bundle.putString(DetallesActivity.CLAVE_FECHA_PEDIDO, fechaPedido);
            bundle.putBoolean(DetallesActivity.CLAVE_BLOQUEO, bloqueo);
            bundle.putString(DetallesActivity.CLAVE_MENSAJES_ALMACENISTA, mensajeAlmacenista);

            Intent intent = new Intent(getContext(), DetallesActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);

            //NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
            //navController.navigate(R.id.detallesFragment,bundle);

            Log.d("Touch", "TouchingLuisda" + numeroCuenta + " " + fechaPedido);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}